 function hideModal() {
     $(".tender-add-backdrop").hide();
     $(".modalEach").hide();
 }

 function showModal() {
     $(".tender-add-backdrop").show();
     $(".modalEach").show();
 }

 $(function () {
     $(".uploadPhotosBody").dropzone({
         url: "/file/post"
     });
 });
