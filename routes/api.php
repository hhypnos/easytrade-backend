<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/', 'AccountController@index');//show all company

    Route::get('company/{any}', 'AccountController@company'); //show specific company

    Route::get('tenders', 'AccountController@tenders');
    Route::get('tender/{any}', 'AccountController@tender');

    Route::get('logout', 'AuthentificationController@logout');

});

Route::group(['middleware' => 'guest:api'], function () {

    Route::get('login', 'AuthentificationController@login')->name('login');
    Route::post('signin', 'AuthentificationController@signin');

    Route::get('registration', 'AuthentificationController@registration')->name('registration');
    Route::post('signup', 'AuthentificationController@signup');

});
