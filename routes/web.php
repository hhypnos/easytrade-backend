<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AccountController@index');//First Page

//send feedback
Route::post('sendcontact', function () {
    $html = "<p>".request("company")."</p><p>".request("phone")."</p><p>".request("email")."</p>";
    Mail::send(array(), array(), function ($message) use ($html) {
  $message->to("info@easytrade.ge")
    ->subject("Subscribe Eeasy Trade")
    ->from(request("email"),"EasyTrade")
    ->setBody($html, 'text/html');
});
    return redirect(URL::to("/"));
});

Route::group(['middleware' => 'auth'], function () {
//Main View
    Route::get('dashboard', 'AccountController@dashboard');//display dashboard
//End Main View

//Company/User Profile(dashboard)
    Route::get('company/{any}', 'AccountController@company'); //show specific company
    Route::get('user/{any}', 'AccountController@user'); //show specific user
//End Company/User Profile(dashboard)
//Tender Control
    Route::get('tenders', 'TenderController@tenders');//show all tenders
    Route::get('tender/{any}', 'TenderController@tender');//show specific tender

    Route::get('addtender', 'TenderController@addTender');//view
    Route::post('addtender', 'TenderController@addTenderDB');//post method

//End Tender Control

//Card Control
    Route::post('addtocard', 'CardController@addToCard');//ajax method
    Route::post('deletefromcard', 'CardController@deleteFromCard');//ajax method
    Route::post('buyitemsfromcard', 'CardController@buyItemsFromCard');//ajax method
    Route::post('buypromotionsfromcard', 'CardController@buyPromotionsFromCard');//ajax method
//End Card Control

//Production Control
    Route::get('orders', 'AccountController@orders');//view (productions)
    Route::get('addproductions', 'AccountController@addProductions');//add productions
    Route::post('importproductions', 'AccountController@addProductionsWithImport');//add productions from excel (post method)
    Route::post('deleteproductions', 'AccountController@deleteProductions');//delete productions(post method)
//End Production Control

//Promotion Control
    Route::get('mypromotions', 'PromotionController@myPromotions');//view
    Route::get('addpromotions', 'PromotionController@addPromotions');//view
    Route::post('addpromotions', 'PromotionController@addPromotionsDB');//post
    Route::post('deletepromotions', 'PromotionController@deletePromotions');//delete promotions(post method)
//End Promotion Control


//Log out from System
    Route::get('logout', 'AuthentificationController@logout');

});
//Authentification Panel
Route::group(['middleware' => 'guest'], function () {

    Route::get('login', 'AuthentificationController@login')->name('login');//view
    Route::post('signin', 'AuthentificationController@signin');//post method

    Route::get('registration', 'AuthentificationController@registration')->name('registration');//view
    Route::post('signup', 'AuthentificationController@signup');//post method

});
