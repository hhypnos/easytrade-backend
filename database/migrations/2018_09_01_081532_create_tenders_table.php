<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userID');
            $table->string('productName');
            $table->float('price');
            $table->float('quantity');
            $table->integer('quantityFormatID');
            $table->string('file');
            $table->date('startTender');
            $table->date('endTender');
            $table->text('description');
            $table->text('paymentDescription');
            $table->text('candidateDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders');
    }
}
