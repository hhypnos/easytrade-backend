<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoldProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_productions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userID');
            $table->integer('productionID');
            $table->string('invoice',15)->unique();
            $table->float('price');
            $table->float('quantity');
            $table->date('orderDate');
            $table->enum('paymentMethod',['cash','transaction','consignation']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_productions');
    }
}
