<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organizationID');
            $table->biginteger('barcode');
            $table->string('image')->nullable();
            $table->string('product_name');
            $table->integer('productionTypeID');
            $table->float('price');
            $table->float('quantity');
            $table->integer('quantityFormatID');
            $table->integer('deleteOrNot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
