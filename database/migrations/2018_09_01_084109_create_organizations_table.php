<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('organizationID')->nullable();
            $table->integer('organization_typeID')->nullable();
            $table->string('organization_name');
            $table->string('organization_slogan')->nullable();
            $table->string('description')->nullable();
            $table->integer('countryID')->nullable();
            $table->integer('cityID')->nullable();
            $table->string('address')->nullable();
            $table->string('zip_code',15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
