<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userID');
            $table->integer('promotionTypeID');
            $table->integer('productionID');
            $table->text('description');
            $table->float('minimalProduct');
            $table->float('salePrecentage');
            $table->date('startDate');
            $table->date('endDate');
            $table->integer('deleteOrNot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
