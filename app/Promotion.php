<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    public function production()
    {
        return $this->belongsTo('App\Production', 'productionID', 'id');
    }
    public function sold_promotions()
    {
        return $this->hasMany('App\Sold_promotion', 'promotionID', 'id');
    }
}
