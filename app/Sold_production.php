<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sold_production extends Model
{
    public function production()
    {
        return $this->belongsTo('App\Production', 'productionID', 'id');
    }
    public function user()
    {
            return $this->belongsTo('App\User', 'userID','id');
    }
}
