<?php
/**
* get enums from db
*
* @param $table
* @param $field
*/
function getEnumValues($table,$field)
{
   $type = \DB::select(\DB::raw('SHOW COLUMNS FROM '.$table.' WHERE Field = "'.$field.'"'))[0]->Type;
   preg_match('/^enum\((.*)\)$/', $type, $matches);
   $values = array();
   foreach(explode(',', $matches[1]) as $value){
       $values[] = trim($value, "'");
   }
   return $values;
}

/**
* translate static texts
*
* @param $word
* @param $lang
*/
function translate($word, $lang)
{
    if(DB::table('translates')->where('langID', $lang)->where('origin',$word)->first()){
        return DB::table('translates')->where('langID', $lang)->where('origin',$word)->first()->translated;
    }else{
        return $word;
    }
}
