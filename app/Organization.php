<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    public function organization_type()
    {
        return $this->hasOne('App\Organization_type', 'typeID', 'organization_typeID');
    }

    public function organization_picture()
    {
        return $this->hasMany('App\Organization_picture', 'organizationID', 'organizationID');
    }

    public function production()
    {
            return $this->hasMany('App\Production', 'organizationID','organizationID')->where('deleteOrNot',0);
    }
    public function city()
    {
            return $this->hasOne('App\City', 'cityID','cityID');
    }
    public function organization_phone()
    {
            return $this->hasOne('App\Organization_phone', 'organizationID','organizationID');
    }
}
