<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $guarded = [];//permission for multiple upload from csv(guarded doesn't except any field, so every field can be fillable)
    public function quantity_format()
    {
            return $this->hasOne('App\Quantity_format','quantityFormatID','quantityFormatID');
    }
    public function organization()
    {
            return $this->belongsTo('App\Organization','organizationID','organizationID');
    }
    public function product_sale()
    {
            return $this->hasOne('App\Sale','productID','id')->orderBy('id','desc');
    }
}
