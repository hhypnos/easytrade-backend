<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'userID', 'id');
    }
    public function quantity_format()
    {
        return $this->hasOne('App\Quantity_format', 'quantityFormatID', 'quantityFormatID');
    }
}
