<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Organization;
use App\T_c;
class AuthentificationController extends Controller
{
    public function login()
    {
        return view('authentification.login');
    }

    public function signin()
    {
        $credentialsWithTin = array('identification_code' => request('username'), 'password'=>request()->get('password'));
        $credentialsWithSu = array('rs_su' => request('username'), 'password'=>request('password'));
        $credentialsWithEmail = array('email' => request('username'), 'password'=>request('password'));
        $rememberMe = false;
            if(request('rememberme') != null){
                $rememberMe = true;
            }

            if(auth()->attempt($credentialsWithTin,$rememberMe)){
                return redirect('dashboard');
            }

            if(auth()->attempt($credentialsWithSu,$rememberMe)){
                return redirect('dashboard');
            }

            if(auth()->attempt($credentialsWithEmail,$rememberMe)){
                return redirect('dashboard');
            }

            return response()->json([
                    'error' => 'Unauthenticated user',
                    'code' => 401,
                    ], 401);
    }

    public function registration()
    {
        return view('authentification.registration');
    }

    public function signup()
    {
        $this->validate(request(),[
            'su'=>'required|unique:users,rs_su',
            'sp'=>'required',
            'tin'=>'required|unique:users,identification_code',
            'userOrCompany'=>'required|in:1,2'
        ]);

        try {

        $wsdl = "https://services.rs.ge/WayBillService/WayBillService.asmx?WSDL";
        $client = new \SoapClient($wsdl,array('soap_version' => SOAP_1_2));
        if($client->chek_service_user(array("su" => request('su'),"sp" => request('sp')))->chek_service_userResult){
            $organizationName = $client->get_name_from_tin(array("su" => request('su'),"sp" => request('sp'), "tin" => request('tin')));

            $user = new User;
            $tc = new T_c;

            $user->userOrCompany = request('userOrCompany');
            $user->identification_code = request('tin');
            $user->rs_su = request('su');
            $user->password = \Hash::make(request('sp'));
            $user->t_c_ID = $tc->orderBy('id','desc')->first()->id; //get last terms and contditions ID

            $organization = new Organization;
            //check if organization exists, if doesn;t than add in db...
            if(!$organization->where('organization_name',$organizationName->get_name_from_tinResult)->exists()){
            $organization->organization_name = $organizationName->get_name_from_tinResult;
            if($organization->count() > 0){
            $organization->organizationID = $organization->orderBy('id','desc')->first()->id;
            }
            $organization->organizationID = $organization->count() + 1;
            $organization->save();

            $user->organization_nameID = $organization->organizationID;
            $user->save();
        }else{
            $user->organization_nameID = $organization->where('organization_name',$organizationName->get_name_from_tinResult)->first()->organizationID;
            $user->save();
        }}else{
                  return response()->json([
                          'error' => 'User not found',
                          'code' => 404,
                      ], 404);
              }

        }catch(Exception $e) {
            echo $e->getMessage();
        }
        \Auth::login($user);
        return redirect('/');
    }

    public function logout()
    {
            auth()->logout();

            return redirect('login');
    }
}
