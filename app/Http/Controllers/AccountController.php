<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Organization;
use App\Tender;
use App\Production;
use App\Sold_production;
use App\Promotion;

use App\User;
use App\Quantity_format;
class AccountController extends Controller
{
//Main Page
    public function index()
    {
        return view('welcome.index');
    }
//Account Main Page(your timeline)
    public function dashboard()
    {
        if(auth()->user()->userOrCompany  == 2){

            $sold_productions = Sold_production::orderBy('id','desc')->where('userID',auth()->user()->id)->get();
            $productions = Production::orderBy('id','desc')->where('deleteOrnot',0)->where('organizationID',auth()->user()->organization_nameID)->get();
            $tenders = Tender::orderBy('created_at','desc')->get();
            $promotions = Promotion::orderBy('id','desc')->where('userID',auth()->user()->id)->get();
            return view("account/companyIndex",compact("sold_productions","productions","tenders","promotions"));
        }
    }
//Accounts
    public function company($company)
    {
        $organization = Organization::where('organizationID',$company)->firstOrFail();

        return view('account.company',compact('organization'));
    }

    public function user($user)
    {
        $user = User::where('id',$user)->firstOrFail();

        return view('account.user',compact('user'));
    }


//productions
    public function orders()
    {
        $productions = Production::orderBy('id','desc')->where('deleteOrnot',0)->get();
        return view('account.orders',compact('productions'));
    }

    public function addProductions()
    {
        return view('account.addproductions');
    }

//Import productions from excel to db
    public function addProductionsWithImport(){

        $file = request()->file('file');
        $fileName = auth()->user()->id.'-'.time().'-'.$file->getClientOriginalName();
        $file->move('public/importTemplate/importedProductions', $fileName);
        $file = public_path('importTemplate/importedProductions/'.$fileName);
        $convertedArray = $this->csvToArray($file);
        if(!is_array($convertedArray)){
            return $convertedArray;
        }
        // dd($convertedArray);
        for ($i = 0; $i < count($convertedArray); $i ++){
            Production::firstOrCreate($convertedArray[$i]);
        }

        return 'Jobi done or what ever';
    }
//END Import productions from excel to db
//custom function to convert csv into array
    function csvToArray($filename = '', $delimiter = ','){
    if (!file_exists($filename) || !is_readable($filename)){
        return false;
    }
    $header = null;
    $header2 = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false){
        while (($row = fgetcsv($handle, 999999, $delimiter)) !== false){
            if (!$header && !$header2){
                $header2 = array();
                array_push($header2,$row[7],$row[8],$row[9],$row[10],$row[11],$row[12],$row[13]);
                unset($row[7]);
                unset($row[8]);
                unset($row[9]);
                unset($row[10]);
                unset($row[11]);
                unset($row[12]);
                unset($row[13]);
                $header = $row;
            }else{
                $row[0] =  auth()->user()->organization->organizationID;//organizationID must be same as authentificated user
                $checkIfQuantityFormatExists = Quantity_format::where('quantityFormatID',$row[6])->first();
                //validate inputs
                if($row[3] == ""){//product_name
                    return 'error '.$header[3].' is empty';
                }elseif( $row[4] == "" || !is_numeric($row[4])){//price
                    return 'error '.$header[4].' is empty or isn"t nummeric or it use comma';
                }elseif($row[5] == "" || !is_numeric($row[5])){//quantity
                    return 'error '.$header[5].' is empty or isn"t nummeric or it use comma';
                }elseif($row[6] == "" || !is_numeric($row[6]) || $checkIfQuantityFormatExists === null){//quantityFormatID
                    return 'error '.$header[6].' is empty or isn"t nummeric or it use comma or formatID doesn"T exists';
                }else{
                    unset($row[7]);
                    unset($row[8]);
                    unset($row[9]);
                    unset($row[10]);
                    unset($row[11]);
                    unset($row[12]);
                    unset($row[13]);
                $data[] = array_combine($header, $row);
                }
                //END validate inputs
            }
        }
        fclose($handle);
    }
    return $data;
}
//END custom function to convert csv into array
    public function deleteProductions()
    {
        $this->validate(request(),[
            'itemID' => 'required|numeric'
        ]);

        $production = Production::find(request('itemID'));
        if(auth()->user()->organization->organizationID == $production->organizationID){

            $production->deleteOrNot = 1;
            $production->save();
            return response()->json([
                        'success' => "Deleted"
                    ]);

        }
        return response()->json([
                    'error' => "Can't find product"
                ]);
    }

}
