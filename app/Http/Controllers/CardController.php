<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Production;
use App\Sold_production;
use App\Promotion;

class CardController extends Controller
{
    public function addToCard()
    {
        $this->validate(request(),[
            'itemID' => 'required|numeric',
        ]);
        //check if production session exists if not add
        if (session()->exists('items.products')) {
            if(in_array(request('itemID'), session()->get('items.products'))){
                return response()->json([
                        'error' => 'Product already in card',
                    ]);
            }
            return session()->push('items.products',request('itemID'));
        }
        return session()->put('items.products',array(request('itemID')));
    }

    public function deleteFromCard()
    {
        $this->validate(request(),[
            'itemKey' => 'required|numeric',
        ]);

        //delete product
        if(request('cardType') == 'product'){
            if (session()->exists('items.products.'.request('itemKey'))) {
                return session()->forget('items.products.'.request('itemKey'));
                }
            }
        return response()->json(['error' => "Product can't found"]);
        }

        public function buyItemsFromCard()
        {
            $this->validate(request(),[
                'itemQuantity' => 'required',
                'itemOrderDate' => 'required',
                'itemPaymentMethod' => 'required'
            ]);
            if (session()->exists('items.products')) {
            foreach(session()->get('items.products') as $key => $card){
                $production = Production::find($card);
                if( $production->quantity < request('itemQuantity')[$key] || $production->quantity == 0){
                    return response()->json([
                                'error' => "# ".$production->id.", ".$production->product_name." Production left ".$production->quantity,
                            ]);
                }
                //check if sale exists
                $promotion = Promotion::orderBy('id','desc')->where('deleteOrNot',0)->where('productionID',$card)->first();
                if($promotion !== null && request('itemQuantity')[$key] >= $promotion->minimalProduct && $promotion->startDate<=date('Y-m-d') && $promotion->endDate>=date('Y-m-d')){
                    $price = $promotion->production->price -(($promotion->production->price / 100) * $promotion->salePrecentage);
                }else{
                    $price = $production->price;
                }
                //how much left

                $production->quantity = $production->quantity - request('itemQuantity')[$key];
                $production->save();
                do{
                    $generateInvoice = rand (10000000000000 , 99999999999999 );
                }while(Sold_production::where('invoice',$generateInvoice)->first() !== null);
                //Sold production
                $sold_production = new Sold_production;
                $sold_production->userID = auth()->user()->id;
                $sold_production->invoice = $generateInvoice;
                $sold_production->productionID = $card;
                $sold_production->price = $price;
                $sold_production->quantity = request('itemQuantity')[$key];
                $sold_production->orderDate = request('itemOrderDate')[$key];
                $sold_production->paymentMethod = request('itemPaymentMethod')[$key];
                $sold_production->save();
            }
            return session()->forget('items.products');
        }
        return response()->json([
                    'error' => "Please add products to buy",
                ]);
        }
}
