<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Tender;
use App\Quantity_format;

class TenderController extends Controller
{
//show all tenders
    public function tenders()
    {
        $tenders = Tender::orderBy('created_at','desc')->get();

        return view('tenders.tenders',compact('tenders'));
    }
//show specific tender
    public function tender($tender)
    {
        $tender = Tender::where('id',$tender)->firstOrFail();

        $tender->quantity_format;
        $tender->user->organization->organization_type;

        return view('tenders.specifictender',compact('tender'));
    }
    //add new tender (view)
    public function addTender()
    {

        $quantityFormats = Quantity_format::get();
        return view('tenders.addtenders',compact('quantityFormats'));
    }
//add new tender (post)
    public function addTenderDB()
    {
        $this->validate(request(),[
            'productName' => 'required|min:4',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'quantityFormatID' => 'required|exists:quantity_formats',
            'file'=>'required|max:4000|mimes:xlsx,pdf,doc,docx',
            'startTender'=>'required|date|date_format:Y-m-d',
            'endTender'=>'required|date|date_format:Y-m-d|after_or_equal:startTender',
            'description' => 'required|min:10',
            'paymentDescription' => 'required|min:10',
            'candidateDescription' => 'required|min:10'
        ]);

        $file = request()->file('file');
        $fileName = auth()->user()->id.'-'.time().'-'.$file->getClientOriginalName();
        $file->move('public/assets/documents', $fileName);

        $tender = new Tender;
        $tender->userID = auth()->user()->id;
        $tender->productName = request('productName');
        $tender->price = request('price');
        $tender->quantity = request('quantity');
        $tender->quantityFormatID = request('quantityFormatID');
        $tender->file = \URL::asset('public/assets/documents/'.$fileName);
        $tender->startTender = request('startTender');
        $tender->endTender = request('endTender');
        $tender->description = request('description');
        $tender->paymentDescription = request('paymentDescription');
        $tender->candidateDescription = request('candidateDescription');
        $tender->save();

        return redirect('/');
    }
}
