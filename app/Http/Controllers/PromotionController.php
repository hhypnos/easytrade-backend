<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Promotion;
use App\Production;
use App\Promotion_type;
use App\Sold_production;

class PromotionController extends Controller
{
    //my promotions
    public function myPromotions()
    {
        $promotions = Promotion::orderBy('id','desc')->where('deleteOrNot',0)->where('userID',auth()->user()->id)->get();
        return view('promotions.mypromotions',compact('promotions'));
    }
    //add new promotion(view)
    public function addPromotions()
    {
        $promotionTypes = Promotion_type::get();
        $productions = Production::orderBy('id','desc')->where('deleteOrNot',0)->where('organizationID',auth()->user()->organization->organizationID)->get();
        return view('promotions.addpromotions',compact('promotionTypes','productions'));
    }
    //add new promotion(post)
    public function addPromotionsDB()
    {
        $this->validate(request(),[
            'promotionType' => 'required|numeric',
            'productionName' => 'required|numeric|exists:productions,id',
            'promotionSale' => 'required|numeric',
            'minimalProduct' => 'required|numeric',
            'description' => 'required|min:25',
            'file'=>'max:4000|mimes:jpg,png,jpeg',
            'startDate'=>'required|date|date_format:Y-m-d',
            'endDate'=>'required|date|date_format:Y-m-d|after_or_equal:startTender'
        ]);
        $promotion = new Promotion;
        $promotion->userID = auth()->user()->id;
        $promotion->promotionType = request('promotionType');
        $promotion->productionID = request('productionName');
        $promotion->description = request('description');
        $promotion->startDate = request('startDate');
        $promotion->endDate = request('endDate');
        $promotion->minimalProduct = request('minimalProduct');
        $promotion->salePrecentage = request('promotionSale');
        $promotion->save();
        return 'promotion added';
    }
    //delete promotion
    public function deletePromotions()
    {
        $this->validate(request(),[
            'itemID' => 'required|numeric'
        ]);

        $promotion = Promotion::find(request('itemID'));
        if(auth()->user()->id == $promotion->userID){

            $promotion->deleteOrNot = 1;
            $promotion->save();
            return response()->json([
                        'success' => "Deleted"
                    ]);

        }
        return response()->json([
                    'error' => "Can't find promotion"
                ]);
    }
}
