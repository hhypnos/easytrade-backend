@extends('layouts.master')
@section('content')
    <!--    orders section-->
  <section id="orders-section-momw" class="section-block">
      <div class="section-block-inner">
          <div class="container">
              <!--  momwodebeli left-->
              <div class="col-md-9 momwodebeli-col">
                  <div class="momwodebeli-lefted">
                      <div class="orders-tab-block">
                          <!-- Nav tabs -->
                          <ul class="tabsnavigation" role="tablist">
                              <li role="presentation" class="active"><a href="#receivedSales" aria-controls="receivedSales" role="tab" data-toggle="tab">პროდუქციის ბაზა</a></li>
                          </ul>
                          <!-- end Nav tabs -->
                          <!-- Tab panes -->
                          <div class="tab-content user-page-tabContent">
                              <!-- panel each-->
                              <div role="tabpanel" class="tab-pane active" id="receivedSales">

                                  <div class="tab-panel-block">
                                      <div class="tab-panel-inner">
                                          <div class="panel-header-filter">
                                              <div class="panel-header-filter-inner">
                                                  <span class="filter-title">სორტირება:</span>
                                                  <span class="sort-by-date active-filter">პროდუქციის  მიხედვით</span>
                                                  <span class="sort-by-name">მომწოდებლის მიხედვით</span>
                                                  <div class="search_block-filtHead">
                                                      <div class="search-floating-bk">
                                                          <input type="text" style="display: none;">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <!--each order-->
                                          @foreach($productions as $production)
                                              <div class="each-order-block">
          <div class="each-order-block-inner">
              <div class="col-md-9 momdodeb-col-nop">
                  <div class="col-md-5">
                      <div class="order-icon-info-def">
                          <div class="order-icon">
                             <img src="{{$production->image ? $production->image : URL::asset('public/assets/img/order-icon.png')}}" alt="">
                          </div>
                          <div class="order-info-det">
                              <div class="order-title">
                                 {{$production->product_name}} <span>{{$production->quantity}}{{$production->quantity_format->quantityFormat}}</span>
                              </div>
                              <div class="order-date">
                                 {{$production->updated_at}}
                              </div>
                          </div>

                      </div>
                      <div class="order-inf-bottomrow">
                          <div class="order-supplier">
                              <div class="top-txt">
                                  მომწოდებელი
                              </div>
                              <div class="bottom-txt">
                                  {{$production->organization->organization_name}}
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="order-supplier">
                          <div class="top-txt">
                              მოცულობა
                          </div>
                          <div class="bottom-txt">
                             <span>{{$production->quantity_format->quantityFormat}}</span>
                          </div>
                      </div>
                      <div class="order-rec-date">
                          <div class="order-supplier">
                              <div class="top-txt">
                                  მიწოდების თარიღი
                              </div>
                              <div class="bottom-txt">
                                  21.07.18
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-3 col-nopd">
                      <div class="order-price">
                          <div class="top-txt">
                              ფასი:
                          </div>
                          <div class="bottom-txt">
                              @if($production->sale =='yes')
                                  {{$production->price -(($production->price / 100) * $production->product_sale->newPrice)}}
                              @else
                                  {{$production->price}}
                              @endif
                               ლარი
                          </div>
                      </div>

                  </div>

              </div>
              <div class="col-md-3 momd-rg-cl-no">
                  <div class="quantity-bk-momw">
                      <div class="top-quant-bk">
                          <span>რაოდენობა:</span>
                          <span class="spinners-momw">{{$production->quantity}}{{$production->quantity_format->quantityFormat}}</span>
                      </div>
                      <div class="in-stocks-momd">
                          მარაგშია
                      </div>
                  </div>

                  <div class="order-more-btn">
                      <button type="button" class="btn-writepers" onclick="addToCard({{$production->id}})">კალათაში დამატება</button>
                  </div>
              </div>
              <div class="cls"></div>
          </div>
          <div class="cls"></div>
      </div>
                                          @endforeach
                                          <!-- end each order-->

                                      </div>
                                  </div>
                              </div>
                              <!-- end panel each-->

                              <!-- panel each-->

                              <!-- end panel each-->
                          </div>
                      </div>
                  </div>
              </div>
              <!--  end momwodebeli left -->

              @include("layouts.productcard")
              </div>
          </div>
      </div>
  </section>
  <!--  end orders section-->
  <!--  end orders section-->
@endsection
