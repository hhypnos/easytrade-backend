@extends('layouts.master')
@section('content')
    <!-- main top section -->
  <section id="main-pg-top" class="section-block momwodebeli-notifications">
      <div class="main-section-in">
          <div class="container">
              <div class="row">
                  <div class="col-md-9 momwodebeli-col momw-top-pg">
                      <!--tenderis productis bloki-->
                      <div id="addNewProductModal" class="addTenderModal modalEach">
                          <div class="addTenderModHeader">
                              ახალი პროდუქციის დამატება
                              <div class="modalImportBtn importedAbsHead">
                                  <a href="{{URL::to('importTemplate/productionsTemplate.csv')}}">თემფლეითი ბაზის დასაიმპორტებლად</a>
                              </div>
                              <form class="" action="{{URL::to('importproductions')}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <input type="file" name="file">
                                  <input type="submit" value="sumit">
                              </form>
                              <br>
                              <br>
                              <br>
                          </div>
                          <div class="addTEnderModalInner">
                              <div class="addTenderModalBody">
                                  <div class="addTenderModalBody_inner">

                                      <!--left-->
                                      <div class="leftedRowModalProd">
                                          <div class="group-forms">
                                              <div class="titleFRmup">
                                                  პროდუქციის ფოტო:
                                              </div>
                                              <div class="uploadPhotosBody">
                                                  <form action="/file-upload" class="dropzone">
                                                      <div class="fallback">
                                                          <input name="file" type="file" multiple />
                                                      </div>
                                                  </form>
                                              </div>
                                          </div>
                                      </div>
                                      <!-- end left-->

                                      <!--right-->
                                      <div class="rightedRowModalProd">
                                          <div class="eachrowmodPRod">
                                              <label for="product-name">პროდუქციის დასახელება:</label>
                                              <input type="text" id="product-name">
                                          </div>
                                          <div class="eachrowmodPRod">
                                              <label for="product-code">პროდუქციის კოდი:</label>
                                              <input type="text" id="product-code" class="inputSmMEdium">
                                          </div>
                                          <div class="eachrowmodPRod">
                                              <label for="metricDigit">საზომი ერთეული:</label>
                                              <input type="text" id="metricDigit" class="inputSmSmall">
                                          </div>
                                          <div class="eachrowmodPRod">
                                              <label for="productPrice">პროდუქციის ფასი:</label>
                                              <input type="text" id="productPrice" class="inputSmSmallExtra">
                                              <span class="extrafield">ლარი</span>
                                          </div>
                                          <div class="eachrowmodPRod">
                                              <label for="prodCategory">კატეგორია:</label>
                                              <select name="prodCategory" id="prodCategory" class="inputSmSmallExtra">
                                                      <option value="val1">საკვები პროდუქტი</option>
                                                     <option value="val2">სასმელი</option>
                                              </select>
                                          </div>
                                          <div class="eachrowmodPRod">
                                              <label for="groupPrices">ფასთა ჯგუფი:</label>
                                              <input type="text" id="groupPrices" class="inputSmSmallExtra">
                                          </div>
                                      </div>
                                      <!-- endright-->
                                      <div class="cls"></div>

                                  </div>
                              </div>

                              <!--footer-->
                              <div class="addTenderModalFooter">
                                  <div class="modalFootLeft">
                                      <a href="#" class="addMoreTender">მეტი პროდუქტის დამატება</a>
                                  </div>
                                  <div class="modalFootRight">
                                      <div class="TenderCanselBtn">
                                          <button onclick="hideModal()">გაუქმება</button>
                                      </div>
                                      <div class="TenderAddButton">
                                          <button onclick="hideModal()">დამატება</button>
                                      </div>
                                  </div>
                                  <div class="cls"></div>
                              </div>
                          </div>
                      </div>
                      <!-- end productis damatebis bloki-->
                      <div class="momwodebeli-lefted">
                          <div class="orders-tab-block">


                              <!-- Nav tabs -->
                              <ul class="tabsnavigation" role="tablist">
                                  <li role="presentation"><a href="#momwmyprods" aria-controls="momwmyprods" role="tab" data-toggle="tab">ჩემი პროდუქცია<span onclick="showModal()" class="add-action"><img src="img/add.png" alt=""></span></a></li>
                                  <li role="presentation"><a href="#receivedSales" aria-controls="receivedSales" role="tab" data-toggle="tab">მიღებული შეკვეთები</a></li>
                                  <li role="presentation" class="active"><a href="#toreceive-orders" aria-controls="toreceive-orders" role="tab" data-toggle="tab">მისაღები შეკვეთები<span class="order-count-sp">(28)</span></a></li>
                              </ul>
                              <!-- end Nav tabs -->
                              <!-- Tab panes -->
                              <div class="tab-content user-page-tabContent">
                                  <!-- panel each-->
                                  <div role="tabpanel" class="tab-pane active" id="momwmyprods">

                                      <div class="tab-panel-block">
                                          <div class="tab-panel-inner">
                                              <div class="panel-header-filter">
                                                  <div class="panel-header-filter-inner">
                                                      <div class="order-check filter-check-all">
                                                          <div class="checkbox">
                                                              <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                          </div>
                                                      </div>
                                                      <span class="filter-title">სორტირება:</span>
                                                      <span class="sort-by-date active-filter">თარიღის შეცვლა</span>
                                                      <span class="sort-by-name">RS.GE ატვირთვა</span>
                                                      <span class="sort-by-name">ექსპორტირება</span>
                                                      <div class="search_block-filtHead">
                                                          <div class="search-floating-bk">
                                                              <input type="text" style="display: none;">
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <!--each order-->
                                              <div class="each-order-block">
                                                  <div class="each-order-block-inner">
                                                      <div class="col-md-10 momdodeb-col-nop">
                                                          <div class="col-md-5">
                                                              <div class="order-icon-info-def">
                                                                  <div class="order-check">
                                                                      <div class="checkbox">
                                                                          <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                                      </div>
                                                                  </div>
                                                                  <div class="order-info-det">
                                                                      <div class="order-title">
                                                                          ზეთი ოლეინა <span>2ლ</span>
                                                                      </div>
                                                                      <div class="order-date">
                                                                          9 ივლისი, 17:00
                                                                      </div>
                                                                  </div>

                                                              </div>
                                                              <div class="order-inf-bottomrow">
                                                                  <div class="order-supplier">
                                                                      <div class="top-txt shps-bg">
                                                                          შპს მაინსკორპ
                                                                      </div>
                                                                      <div class="bottom-txt shps-sm">
                                                                          თბილისი, ჭავჭავაძის 20
                                                                      </div>
                                                                      <div class="sub-buttom">
                                                                          5600002124
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="col-md-4">
                                                              <div class="order-supplier">
                                                                  <div class="top-txt">
                                                                      მიღების თარიღი
                                                                  </div>
                                                                  <div class="bottom-txt color-txt-bl">
                                                                      13:44, 31.07.2018
                                                                  </div>
                                                              </div>
                                                              <div class="order-rec-date">
                                                                  <div class="order-supplier">
                                                                      <div class="top-txt">
                                                                          მიტანა
                                                                      </div>
                                                                      <div class="bottom-txt color-txt-bl">
                                                                          02.09.2018
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="invoice-upload">
                                                                  <span>010085402</span>ატვირთვა
                                                              </div>
                                                          </div>
                                                          <div class="col-md-3 col-nopd">
                                                              <div class="upload-pdf">
                                                                  PDF ექსპორტი
                                                              </div>
                                                              <div class="upload-xls">
                                                                  XLS ექსპორტი
                                                              </div>
                                                          </div>

                                                      </div>
                                                      <div class="col-md-2 momd-rg-cl-no">
                                                          <div class="edit-delete-btns mom-del-btns">
                                                              <a href="#" class="editBTn">edit</a>
                                                              <a href="#" class="deleteBTn">delete</a>

                                                          </div>
                                                      </div>
                                                      <div class="cls"></div>
                                                  </div>
                                                  <div class="cls"></div>
                                              </div>
                                              <!-- end each order-->

                                              <!--each order-->
                                              <div class="each-order-block">
                                                  <div class="each-order-block-inner">
                                                      <div class="col-md-10 momdodeb-col-nop">
                                                          <div class="col-md-5">
                                                              <div class="order-icon-info-def">
                                                                  <div class="order-check">
                                                                      <div class="checkbox">
                                                                          <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                                      </div>
                                                                  </div>
                                                                  <div class="order-info-det">
                                                                      <div class="order-title">
                                                                          ზეთი ოლეინა <span>2ლ</span>
                                                                      </div>
                                                                      <div class="order-date">
                                                                          9 ივლისი, 17:00
                                                                      </div>
                                                                  </div>

                                                              </div>
                                                              <div class="order-inf-bottomrow">
                                                                  <div class="order-supplier">
                                                                      <div class="top-txt shps-bg">
                                                                          შპს მაინსკორპ
                                                                      </div>
                                                                      <div class="bottom-txt shps-sm">
                                                                          თბილისი, ჭავჭავაძის 20
                                                                      </div>
                                                                      <div class="sub-buttom">
                                                                          5600002124
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="col-md-4">
                                                              <div class="order-supplier">
                                                                  <div class="top-txt">
                                                                      მიღების თარიღი
                                                                  </div>
                                                                  <div class="bottom-txt color-txt-bl">
                                                                      13:44, 31.07.2018
                                                                  </div>
                                                              </div>
                                                              <div class="order-rec-date">
                                                                  <div class="order-supplier">
                                                                      <div class="top-txt">
                                                                          მიტანა
                                                                      </div>
                                                                      <div class="bottom-txt color-txt-bl">
                                                                          02.09.2018
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="invoice-upload">
                                                                  <span>010085402</span>ატვირთვა
                                                              </div>
                                                          </div>
                                                          <div class="col-md-3 col-nopd">
                                                              <div class="upload-pdf">
                                                                  PDF ექსპორტი
                                                              </div>
                                                              <div class="upload-xls">
                                                                  XLS ექსპორტი
                                                              </div>
                                                          </div>

                                                      </div>
                                                      <div class="col-md-2 momd-rg-cl-no">
                                                          <div class="edit-delete-btns mom-del-btns">
                                                              <a href="#" class="editBTn">edit</a>
                                                              <a href="#" class="deleteBTn">delete</a>

                                                          </div>
                                                      </div>
                                                      <div class="cls"></div>
                                                  </div>
                                                  <div class="cls"></div>
                                              </div>
                                              <!-- end each order-->

                                          </div>
                                      </div>
                                  </div>
                                  <!-- end panel each-->

                                  <!-- panel each-->
                                  <div role="tabpanel" class="tab-pane" id="receivedSales">

                                      <div class="tab-panel-inner">
                                          <div class="panel-header-filter">
                                              <div class="panel-header-filter-inner">
                                                  <div class="order-check filter-check-all">
                                                      <div class="checkbox">
                                                          <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                      </div>
                                                  </div>
                                                  <span class="filter-title">სორტირება:</span>
                                                  <span class="sort-by-date active-filter">თარიღის შეცვლა</span>
                                                  <span class="sort-by-name">RS.GE ატვირთვა</span>
                                                  <span class="sort-by-name">ექსპორტირება</span>
                                                  <div class="search_block-filtHead">
                                                      <div class="search-floating-bk">
                                                          <input type="text" style="display: none;">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <!--each order-->
                                          <div class="each-order-block">
                                              <div class="each-order-block-inner">
                                                  <div class="col-md-10 momdodeb-col-nop">
                                                      <div class="col-md-5">
                                                          <div class="order-icon-info-def">
                                                              <div class="order-check">
                                                                  <div class="checkbox">
                                                                      <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                                  </div>
                                                              </div>
                                                              <div class="order-info-det">
                                                                  <div class="order-title">
                                                                      ზეთი ოლეინა <span>2ლ</span>
                                                                  </div>
                                                                  <div class="order-date">
                                                                      9 ივლისი, 17:00
                                                                  </div>
                                                              </div>

                                                          </div>
                                                          <div class="order-inf-bottomrow">
                                                              <div class="order-supplier">
                                                                  <div class="top-txt shps-bg">
                                                                      შპს მაინსკორპ
                                                                  </div>
                                                                  <div class="bottom-txt shps-sm">
                                                                      თბილისი, ჭავჭავაძის 20
                                                                  </div>
                                                                  <div class="sub-buttom">
                                                                      5600002124
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-4">
                                                          <div class="order-supplier">
                                                              <div class="top-txt">
                                                                  მიღების თარიღი
                                                              </div>
                                                              <div class="bottom-txt color-txt-bl">
                                                                  13:44, 31.07.2018
                                                              </div>
                                                          </div>
                                                          <div class="order-rec-date">
                                                              <div class="order-supplier">
                                                                  <div class="top-txt">
                                                                      მიტანა
                                                                  </div>
                                                                  <div class="bottom-txt color-txt-bl">
                                                                      02.09.2018
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="invoice-upload">
                                                              <span>010085402</span>ატვირთვა
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3 col-nopd">
                                                          <div class="upload-pdf">
                                                              PDF ექსპორტი
                                                          </div>
                                                          <div class="upload-xls">
                                                              XLS ექსპორტი
                                                          </div>
                                                      </div>

                                                  </div>
                                                  <div class="col-md-2 momd-rg-cl-no">
                                                      <div class="edit-delete-btns mom-del-btns">
                                                          <a href="#" class="editBTn">edit</a>
                                                          <a href="#" class="deleteBTn">delete</a>

                                                      </div>
                                                  </div>
                                                  <div class="cls"></div>
                                              </div>
                                              <div class="cls"></div>
                                          </div>
                                          <!-- end each order-->

                                      </div>
                                  </div>
                                  <!-- end panel each-->

                                  <!-- panel each-->
                                  <div role="tabpanel" class="tab-pane" id="toreceive-orders">

                                      <div class="tab-panel-inner">
                                          <div class="panel-header-filter">
                                              <div class="panel-header-filter-inner">
                                                  <div class="order-check filter-check-all">
                                                      <div class="checkbox">
                                                          <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                      </div>
                                                  </div>
                                                  <span class="filter-title">სორტირება:</span>
                                                  <span class="sort-by-date active-filter">თარიღის შეცვლა</span>
                                                  <span class="sort-by-name">RS.GE ატვირთვა</span>
                                                  <span class="sort-by-name">ექსპორტირება</span>
                                                  <div class="search_block-filtHead">
                                                      <div class="search-floating-bk">
                                                          <input type="text" style="display: none;">
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <!--each order-->
                                          <div class="each-order-block">
                                              <div class="each-order-block-inner">
                                                  <div class="col-md-10 momdodeb-col-nop">
                                                      <div class="col-md-5">
                                                          <div class="order-icon-info-def">
                                                              <div class="order-check">
                                                                  <div class="checkbox">
                                                                      <label>
                                                                          <input type="checkbox" value="">
                                                                          <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                                  </div>
                                                              </div>
                                                              <div class="order-info-det">
                                                                  <div class="order-title">
                                                                      ზეთი ოლეინა <span>2ლ</span>
                                                                  </div>
                                                                  <div class="order-date">
                                                                      9 ივლისი, 17:00
                                                                  </div>
                                                              </div>

                                                          </div>
                                                          <div class="order-inf-bottomrow">
                                                              <div class="order-supplier">
                                                                  <div class="top-txt shps-bg">
                                                                      შპს მაინსკორპ
                                                                  </div>
                                                                  <div class="bottom-txt shps-sm">
                                                                      თბილისი, ჭავჭავაძის 20
                                                                  </div>
                                                                  <div class="sub-buttom">
                                                                      5600002124
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-4">
                                                          <div class="order-supplier">
                                                              <div class="top-txt">
                                                                  მიღების თარიღი
                                                              </div>
                                                              <div class="bottom-txt color-txt-bl">
                                                                  13:44, 31.07.2018
                                                              </div>
                                                          </div>
                                                          <div class="order-rec-date">
                                                              <div class="order-supplier">
                                                                  <div class="top-txt">
                                                                      მიტანა
                                                                  </div>
                                                                  <div class="bottom-txt color-txt-bl">
                                                                      02.09.2018
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="invoice-upload">
                                                              <span>010085402</span>ატვირთვა
                                                          </div>
                                                      </div>
                                                      <div class="col-md-3 col-nopd">
                                                          <div class="upload-pdf">
                                                              PDF ექსპორტი
                                                          </div>
                                                          <div class="upload-xls">
                                                              XLS ექსპორტი
                                                          </div>
                                                      </div>

                                                  </div>
                                                  <div class="col-md-2 momd-rg-cl-no">
                                                      <div class="edit-delete-btns mom-del-btns">
                                                          <a href="#" class="editBTn">edit</a>
                                                          <a href="#" class="deleteBTn">delete</a>

                                                      </div>
                                                  </div>
                                                  <div class="cls"></div>
                                              </div>
                                              <div class="cls"></div>
                                          </div>
                                          <!-- end each order-->

                                      </div>
                                  </div>
                                  <!-- end panel each-->
                              </div>
                          </div>
                      </div>

                      <div class="main-left-block-bottom">
                          <div class="col-md-7 reporting-bot-bk">
                              <div class="receivedOrders-title">
                                  რეპორტინგი
                              </div>
                              <div class="reporting-body">
                                  <div class="reporting-body-inner">
                                      <img src="img/reporting.png" class="img-responsive" alt="">
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-5 supplier-col">
                              <div class="receivedOrders-title">
                                  შეკვეთები
                              </div>
                              <div class="supplier-body-bt">
                                  <div class="supplier-body-bt-inner">
                                      <!--each-->
                                      <div class="momw-orders-sect">
                                          <div class="supplier-each active-supplier">
                                              <div class="supplier-each-inner">
                                                  <div class="supplier-status">
                                                      ახალი
                                                  </div>
                                                  <div class="supplier-title">
                                                      შპს გიორგი 888
                                                  </div>
                                                  <div class="supplier-imported-quantity">
                                                      2310
                                                  </div>
                                              </div>
                                              <div class="cls"></div>
                                          </div>
                                          <div class="order-momw-subTxt">
                                              <div class="order-momw-subTxt-inner">
                                                  ზოგიერთ სააგენტოების 90 გაავრცელა ტექსტი სახელწოდებით "ყვითელი ტრამვაის"
                                              </div>
                                          </div>
                                      </div>
                                      <!-- end each-->
                                      <!--each-->
                                      <div class="momw-orders-sect">
                                          <div class="supplier-each">
                                              <div class="supplier-each-inner">
                                                  <div class="supplier-status status-experienced">
                                                      ძველი
                                                  </div>
                                                  <div class="supplier-title">
                                                      შპს გიორგი 888
                                                  </div>
                                                  <div class="supplier-imported-quantity">
                                                      2310
                                                  </div>
                                              </div>
                                              <div class="cls"></div>
                                          </div>
                                          <div class="order-momw-subTxt">
                                              <div class="order-momw-subTxt-inner">
                                                  ზოგიერთ სააგენტოების 90 გაავრცელა ტექსტი სახელწოდებით "ყვითელი ტრამვაის"
                                              </div>
                                          </div>
                                      </div>
                                      <!-- end each-->
                                      <div class="more-orders-supplier">
                                          <button>მეტი შეკვეთა</button>
                                      </div>
                                      <div class="cls"></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- end left

                  <!-- right-->
                  <div class="col-md-3 notifications-block tender-bk-side">
                      <div class="main-right-block">
                          <div class="notifications-block">
                              <div class="notifications-block-inner">
                                  <div class="notific-title">
                                      შეტყობინებები
                                  </div>
                                  <div class="notific-filter-head">
                                      <div class="notific-filter-head-inner">
                                          <!-- Nav tabs -->
                                          <ul class="tabsnavigation" role="tablist">
                                              <li role="presentation" class="active"><a href="#chatTab" aria-controls="chatTab" role="tab" data-toggle="tab">პრიორიტეტული</a></li>
                                              <li role="presentation"><a href="#notific-tab" aria-controls="notific-tab" role="tab" data-toggle="tab">ისტორია</a></li>
                                          </ul>
                                          <!-- end Nav tabs -->
                                      </div>
                                  </div>
                                  <div class="notifications-body">
                                      <!-- Tab panes -->
                                      <div class="tab-content user-page-tabContent">
                                          <!-- panel each-->
                                          <div role="tabpanel" class="tab-pane active" id="chatTab">

                                              <div class="tab-panel-block">
                                                  <div class="tab-panel-inner">

                                                      <!--each order-->
                                                      <div class="each-order-block notification-each notification-each-active">
                                                          <div class="each-order-block-inner">
                                                              <div class="col-md-12">
                                                                  <div class="notification-in-sm-bd">
                                                                      <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                      <div class="sub-message">
                                                                          ახალი შემოთავაზება
                                                                      </div>
                                                                      <div class="cls"></div>
                                                                  </div>
                                                              </div>
                                                              <div class="cls"></div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <!-- end each order-->
                                                      <!--each order-->
                                                      <div class="each-order-block notification-each ">
                                                          <div class="each-order-block-inner">
                                                              <div class="col-md-12">
                                                                  <div class="notification-in-sm-bd">
                                                                      <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                      <div class="sub-message">
                                                                          ახალი შემოთავაზება
                                                                      </div>
                                                                      <div class="cls"></div>
                                                                  </div>
                                                              </div>
                                                              <div class="cls"></div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <!-- end each order-->
                                                      <!--each order-->
                                                      <div class="each-order-block notification-each ">
                                                          <div class="each-order-block-inner">
                                                              <div class="col-md-12">
                                                                  <div class="notification-in-sm-bd">
                                                                      <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                      <div class="sub-message">
                                                                          ახალი შემოთავაზება
                                                                      </div>
                                                                      <div class="cls"></div>
                                                                  </div>
                                                              </div>
                                                              <div class="cls"></div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <!-- end each order-->
                                                      <!--each order-->
                                                      <div class="each-order-block notification-each ">
                                                          <div class="each-order-block-inner">
                                                              <div class="col-md-12">
                                                                  <div class="notification-in-sm-bd">
                                                                      <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                      <div class="sub-message">
                                                                          ახალი შემოთავაზება
                                                                      </div>
                                                                      <div class="cls"></div>
                                                                  </div>
                                                              </div>
                                                              <div class="cls"></div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <!-- end each order-->

                                                  </div>
                                              </div>
                                          </div>
                                          <!-- end panel each-->

                                          <!-- panel each-->
                                          <div role="tabpanel" class="tab-pane" id="notific-tab">

                                              <div class="tab-panel-inner">

                                                  <!--each order-->
                                                  <div class="each-order-block notification-each notification-each-active">
                                                      <div class="each-order-block-inner">
                                                          <div class="col-md-12">
                                                              <div class="notification-in-sm-bd">
                                                                  <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                  <div class="sub-message">
                                                                      ახალი შემოთავაზება
                                                                  </div>
                                                                  <div class="cls"></div>
                                                              </div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <div class="cls"></div>
                                                  </div>
                                                  <!-- end each order-->

                                                  <!--each order-->
                                                  <div class="each-order-block notification-each notification-each-active">
                                                      <div class="each-order-block-inner">
                                                          <div class="col-md-12">
                                                              <div class="notification-in-sm-bd">
                                                                  <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                  <div class="sub-message">
                                                                      ახალი შემოთავაზება
                                                                  </div>
                                                                  <div class="cls"></div>
                                                              </div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <div class="cls"></div>
                                                  </div>
                                                  <!-- end each order-->

                                                  <!--each order-->
                                                  <div class="each-order-block notification-each notification-each-active">
                                                      <div class="each-order-block-inner">
                                                          <div class="col-md-12">
                                                              <div class="notification-in-sm-bd">
                                                                  <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                  <div class="sub-message">
                                                                      ახალი შემოთავაზება
                                                                  </div>
                                                                  <div class="cls"></div>
                                                              </div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <div class="cls"></div>
                                                  </div>
                                                  <!-- end each order-->

                                                  <!--each order-->
                                                  <div class="each-order-block notification-each notification-each-active">
                                                      <div class="each-order-block-inner">
                                                          <div class="col-md-12">
                                                              <div class="notification-in-sm-bd">
                                                                  <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                  <div class="sub-message">
                                                                      ახალი შემოთავაზება
                                                                  </div>
                                                                  <div class="cls"></div>
                                                              </div>
                                                          </div>
                                                          <div class="cls"></div>
                                                      </div>
                                                      <div class="cls"></div>
                                                  </div>
                                                  <!-- end each order-->

                                              </div>
                                          </div>
                                          <!-- end panel each-->
                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="receivedOrders-title">
                          ტენდერი
                      </div>
                      <div class="tender-body-side">
                          <div class="tender-body-side-inner">
                              <!--each-->
                              <div class="tender-each active-tender">
                                  <div class="tender-each-inner">
                                      <div class="tender-each-head">
                                          <div class="tender-each-head-in">
                                              თივის შესყიდვა
                                          </div>
                                      </div>
                                      <div class="tender-each-bottom">
                                          <div class="tender-each-bottom-in">
                                              <span class="tp-bg-med">წარმდგენი</span>
                                              <span class="tp-sm-md">შინაგან საქმეთა სამინისტრო</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!--end each-->
                              <!--each-->
                              <div class="tender-each active-tender">
                                  <div class="tender-each-inner">
                                      <div class="tender-each-head">
                                          <div class="tender-each-head-in">
                                              თივის შესყიდვა
                                          </div>
                                      </div>
                                      <div class="tender-each-bottom">
                                          <div class="tender-each-bottom-in">
                                              <span class="tp-bg-med">წარმდგენი</span>
                                              <span class="tp-sm-md">შინაგან საქმეთა სამინისტრო</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <!--end each-->
                              <div class="more-tender-btn">
                                  <button>მეტი შეკვეთა</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--end  main top section -->

@endsection
