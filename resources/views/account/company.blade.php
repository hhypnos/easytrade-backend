@extends('layouts.master')
@section('content')
    <section id="user-information-section" class="section-block">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <!--left-->
                <div class="col-md-7">
                    <div class="row row-userPersonal">
                        <!--avatar-->
                        <div class="col-md-4">
                            <div class="user-avatar">
                                <img src="{{$organization->organization_picture[0]->image}}" alt="">
                            </div>
                        </div>
                        <!--end avatar-->
                        <!--user-personal info-->
                        <div class="col-md-4 col-userPersonal">
                            <div class="user-personal-info">
                                <div class="user-title">
                                    {{$organization->organization_name}}
                                </div>
                                <div class="user-type-comp">
                                    {{$organization->organization_type->organization_type}}

                                </div>
                                <div class="user-secondary-info">
                                    {{$organization->organization_slogan}}
                                </div>
                                <div class="user-additional-info">
                                    {{count($organization->production)}} პროდუქტი პლატფორმაზე
                                </div>
                            </div>
                        </div>
                        <!--end user-personal info-->
                        <!--contact info-->
                        <div class="col-md-4 col-userPersonal">
                            <div class="user-contact-ifno">
                                <div class="contact-title">
                                    საკონტაქტო ინფრომაცია:
                                </div>
                                <div class="contact-country">
                                    {{$organization->city->country->country}}
                                </div>
                                <div class="contact-city">
                                    {{$organization->city->city}}
                                </div>
                                <div class="contact-street">
                                    {{$organization->address}}
                                </div>
                                <div class="contact-zip-phone">
                                    <span class="zip-code">{{$organization->zip_code}}, </span>
                                    <span class="phone">  {{$organization->organization_phone->phone}}</span>
                                </div>
                            </div>
                        </div>
                        <!--contact info-->
                    </div>
                </div>
                <!--end left-->
                <!--right-->
                <div class="col-md-5 col-userPersonal">
                    <div class="button-block-def">
                        <button class="btn-writepers">პირადი მიმოწერა</button>
                    </div>
                </div>
                <!--end right-->
            </div>
        </div>
    </div>
    </section>
    <section id="orders-section" class="section-block">
    <div class="section-block-inner">
        <div class="container">
            <!--  momwodebeli left-->
            <div class="col-md-9 momwodebeli-col">
                <div class="momwodebeli-lefted">
                    <div class="orders-tab-block">
                        <!-- Nav tabs -->
                        <ul class="tabsnavigation" role="tablist">
                            <li role="presentation" class="active"><a href="#sales-block" aria-controls="sales-block" role="tab" data-toggle="tab">ფასდაკლებები</a></li>
                            <li role="presentation"><a href="#product-base" aria-controls="product-base" role="tab" data-toggle="tab">პროდუქციის ბაზა</a></li>
                        </ul>
                        <!-- end Nav tabs -->
                        <!-- Tab panes -->
                        <div class="tab-content user-page-tabContent">
                            <!-- panel each-->
                            <div role="tabpanel" class="tab-pane active" id="sales-block">

                                <div class="tab-panel-block">
                                    <div class="tab-panel-inner">
                                        <div class="panel-header-filter">
                                            <div class="panel-header-filter-inner">
                                                <span class="filter-title">სორტირება:</span>
                                                <span class="sort-by-date active-filter">თარიღის მიხედვით</span>
                                                <span class="sort-by-name">სახელის მიხედვით</span>
                                                <div class="search_block-filtHead">
                                                    <div class="search-floating-bk">
                                                        <input type="text" style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--each order-->
                                        @foreach($organization->production as $production)
                                            @if($production->sale =='yes')
                                        <div class="each-order-block">
                                            <div class="each-order-block-inner">
                                                <div class="col-md-8 momdodeb-col-nop">
                                                    <div class="col-md-6">
                                                        <div class="order-icon-info-def">
                                                            <div class="order-icon">
                                                                <img src="{{$production->image ? $production->image : URL::asset('public/assets/img/order-icon.png')}}" alt="">
                                                            </div>
                                                            <div class="order-info-det">
                                                                <div class="order-title">
                                                                {{$production->product_name}} <span>{{$production->quantity}}{{$production->quantity_format->quantityFormat}}</span>
                                                                </div>
                                                                <div class="order-date">
                                                                    {{$production->updated_at}}
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="order-supplier">
                                                            <div class="top-txt">
                                                                მომწოდებელი:
                                                            </div>
                                                            <div class="bottom-txt">
                                                                {{$production->organization->organization_name}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="order-price">
                                                            <div class="top-txt">
                                                                ფასი:
                                                            </div>
                                                            <div class="bottom-txt">
                                                                {{$production->price -(($production->price / 100) * $production->product_sale->newPrice)}}ლარი
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4 momd-rg-cl-no">
                                                    <div class="order-more-btn">
                                                        <button type="button" class="btn-writepers" onclick="addToCard({{$production->id}})" >კალათაში დამატება</button>
                                                    </div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        @endif
                                    @endforeach
                                        <!-- end each order-->

                                    </div>
                                </div>
                            </div>
                            <!-- end panel each-->

                            <!-- panel each-->
                            <div role="tabpanel" class="tab-pane" id="product-base">

                                <div class="tab-panel-inner">
                                    <div class="panel-header-filter">
                                        <div class="panel-header-filter-inner">
                                            <span class="filter-title">სორტირება:</span>
                                            <span class="sort-by-date">თარიღის მიხედვით</span>
                                            <span class="sort-by-name">სახელის მიხედვით</span>
                                        </div>
                                    </div>
                                    <!--each order-->
                                    @foreach($organization->production as $production)
                                    <div class="each-order-block" id="production-{{$production->id}}">
                                        <div class="each-order-block-inner">
                                            <div class="col-md-8 momdodeb-col-nop">
                                                <div class="col-md-6">
                                                    <div class="order-icon-info-def">
                                                        <div class="order-icon">
                                                            <img src="{{$production->image ? $production->image : URL::asset('public/assets/img/order-icon.png')}}" alt="">
                                                        </div>
                                                        <div class="order-info-det">
                                                            <div class="order-title">
                                                                {{$production->product_name}} <span>{{$production->quantity}}{{$production->quantity_format->quantityFormat}}</span>
                                                            </div>
                                                            <div class="order-date">
                                                                {{$production->updated_at}}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="order-supplier">
                                                        <div class="top-txt">
                                                            მომწოდებელი:
                                                        </div>
                                                        <div class="bottom-txt">
                                                            {{$production->organization->organization_name}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="order-price">
                                                        <div class="top-txt">
                                                            ფასი:
                                                        </div>
                                                        <div class="bottom-txt">
                                                            @if($production->sale =='yes')
                                                                {{$production->price -(($production->price / 100) * $production->product_sale->newPrice)}}
                                                            @else
                                                                {{$production->price}}
                                                            @endif
                                                             ლარი
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4 momd-rg-cl-no">
                                                <div class="order-more-btn">
                                                    @if(auth()->user()->organization->organizationID == $organization->organizationID)
                                                              <div class="edit-delete-btns">
                                                                  <a href="#" class="editBTn">edit</a>
                                                                  <a href="#" class="deleteBTn" onclick="deleteProduct({{$production->id}})">delete</a>
                                                              </div>
                                                    @else
                                                    <button type="button" class="btn-writepers" onclick="addToCard({{$production->id}})">კალათაში დამატება</button>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        <div class="cls"></div>
                                    </div>
                                    @endforeach
                                    <!-- end each order-->

                                </div>
                            </div>
                            <!-- end panel each-->
                        </div>
                    </div>
                </div>
            </div>
            <!--  end momwodebeli left -->

            @include('layouts.productcard')
            <script type="text/javascript">
                function deleteProduct(item){
                         var CSRF_TOKEN = "{{csrf_token()}}";
                             $.ajax({
                                 /* the route pointing to the post function */
                                 url: '{{URL::to('deleteproductions')}}',
                                 type: 'POST',
                                 /* send the csrf-token and the input to the controller */
                                 data: {_token: CSRF_TOKEN, itemID:parseInt(item)},
                                 /* remind that 'data' is the response of the AjaxController */
                                 success: function (data) {
                                     if(data['error'] != undefined){
                                         return document.getElementById('error').innerHTML =
                                         '<div class="form-group "><div class="alert alert-danger"><ul><li>'+data['error']+'</li></ul></div> </div>';
                                    }
                                    // location.reload();
                                    $("#production-"+item).remove();
                                 }
                             });
                         }
            </script>
            </div>
        </div>
    </div>
    </section>
@endsection
