@extends('layouts.master')
@section('content')
    <section id="main-pg-top" class="section-block momwodebeli-notifications">
    <div class="main-section-in">
        <div class="container">
            <div class="row">
                <div class="col-md-9 momwodebeli-col momw-top-pg">
                    <div class="momwodebeli-lefted">
                        <div class="orders-tab-block">
                            <!-- Nav tabs -->
                            <ul class="tabsnavigation" role="tablist">
                                <li role="presentation"><a href="#momwmyprods" aria-controls="momwmyprods" role="tab" data-toggle="tab">ჩემი პროდუქცია<span class="add-action"><img src="{{URL::asset('public/assets/img/add.png')}}" alt=""></span></a></li>
                                <li role="presentation"><a href="#receivedSales" aria-controls="receivedSales" role="tab" data-toggle="tab">მიღებული შეკვეთები</a></li>
                                <li role="presentation" class="active"><a href="#toreceive-orders" aria-controls="toreceive-orders" role="tab" data-toggle="tab">მისაღები შეკვეთები<span class="order-count-sp">(28)</span></a></li>
                            </ul>
                            <!-- end Nav tabs -->
                            <!-- Tab panes -->
                            <div class="tab-content user-page-tabContent">
                                <!-- panel each-->
                                <div role="tabpanel" class="tab-pane active" id="momwmyprods">

                                    <div class="tab-panel-block">
                                        <div class="tab-panel-inner">
                                            <div class="panel-header-filter">
                                                <div class="panel-header-filter-inner">
                                                    <div class="order-check filter-check-all">
                                                        <div class="checkbox">
                                                            <label>
                                                                        <input type="checkbox" value="">
                                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                      </label>
                                                        </div>
                                                    </div>
                                                    <span class="filter-title">სორტირება:</span>
                                                    <span class="sort-by-date active-filter">თარიღის შეცვლა</span>
                                                    <span class="sort-by-name">RS.GE ატვირთვა</span>
                                                    <span class="sort-by-name">ექსპორტირება</span>
                                                    <div class="search_block-filtHead">
                                                        <div class="search-floating-bk">
                                                            <input type="text" style="display: none;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--each order-->

                                            @foreach ($productions as $key => $production)
                                                <div class="each-order-block">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-10 momdodeb-col-nop">
                                                            <div class="col-md-5">
                                                                <div class="order-icon-info-def">
                                                                    <div class="order-check">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                            <input type="checkbox" value="">
                                                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                          </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="order-info-det">
                                                                        <div class="order-title">
                                                                            {{$production->product_name}}
                                                                            <span>
                                                                                {{$production->quantity}}{{$production->quantity_format->quantityFormat}}
                                                                            </span>
                                                                        </div>
                                                                        <div class="order-date">
                                                                            {{$production->created_at}}
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="order-inf-bottomrow">
                                                                    <div class="order-supplier">
                                                                        <div class="top-txt shps-bg">
                                                                            {{$production->organization['organization_name']}}
                                                                        </div>
                                                                        <div class="bottom-txt shps-sm">
                                                                            {{$production->organization->city->city}},{{$production->organization['address']}}
                                                                        </div>
                                                                        <div class="sub-buttom">
                                                                            {{$production->organization->organization_phone['phone']}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-nopd">
                                                                <div class="upload-pdf">
                                                                    PDF ექსპორტი
                                                                </div>
                                                                <div class="upload-xls">
                                                                    XLS ექსპორტი
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-2 momd-rg-cl-no">
                                                            <div class="edit-delete-btns mom-del-btns">
                                                                <a href="#" class="editBTn">edit</a>
                                                                <a href="#" class="deleteBTn">delete</a>

                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                            @endforeach
                                            <!-- end each order-->

                                        </div>
                                    </div>
                                </div>
                                <!-- end panel each-->

                                <!-- panel each-->
                                <div role="tabpanel" class="tab-pane" id="receivedSales">

                                    <div class="tab-panel-inner">
                                        <div class="panel-header-filter">
                                            <div class="panel-header-filter-inner">
                                                <div class="order-check filter-check-all">
                                                    <div class="checkbox">
                                                        <label>
                                                                        <input type="checkbox" value="">
                                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                      </label>
                                                    </div>
                                                </div>
                                                <span class="filter-title">სორტირება:</span>
                                                <span class="sort-by-date active-filter">თარიღის შეცვლა</span>
                                                <span class="sort-by-name">RS.GE ატვირთვა</span>
                                                <span class="sort-by-name">ექსპორტირება</span>
                                                <div class="search_block-filtHead">
                                                    <div class="search-floating-bk">
                                                        <input type="text" style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--each order-->
                                        @foreach ($sold_productions as $key => $sold_production)
                                            <div class="each-order-block">
                                                <div class="each-order-block-inner">
                                                    <div class="col-md-10 momdodeb-col-nop">
                                                        <div class="col-md-5">
                                                            <div class="order-icon-info-def">
                                                                <div class="order-check">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                        <input type="checkbox" value="">
                                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                      </label>
                                                                    </div>
                                                                </div>
                                                                <div class="order-info-det">
                                                                    <div class="order-title">
                                                                        {{$sold_production->production->product_name}}
                                                                        <span>
                                                                            {{$sold_production->production->quantity}}{{$sold_production->production->quantity_format->quantityFormat}}
                                                                        </span>
                                                                    </div>
                                                                    <div class="order-date">
                                                                        {{$sold_production->production->created_at}}
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="order-inf-bottomrow">
                                                                <div class="order-supplier">
                                                                    <div class="top-txt shps-bg">
                                                                        {{$sold_production->user->organization['organization_name']}}
                                                                    </div>
                                                                    <div class="bottom-txt shps-sm">
                                                                        {{$sold_production->user->organization->city->city}},{{$sold_production->user->organization['address']}}
                                                                    </div>
                                                                    <div class="sub-buttom">
                                                                        {{$sold_production->user->organization->organization_phone['phone']}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="order-supplier">
                                                                <div class="top-txt">
                                                                    მიღების თარიღი
                                                                </div>
                                                                <div class="bottom-txt color-txt-bl">
                                                                    {{$sold_production->created_at}}
                                                                </div>
                                                            </div>
                                                            <div class="order-rec-date">
                                                                <div class="order-supplier">
                                                                    <div class="top-txt">
                                                                        მიტანა
                                                                    </div>
                                                                    <div class="bottom-txt color-txt-bl">
                                                                        {{$sold_production->orderDate}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="invoice-upload">
                                                                <span>{{$sold_production->invoice}}</span>ატვირთვა
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-nopd">
                                                            <div class="upload-pdf">
                                                                PDF ექსპორტი
                                                            </div>
                                                            <div class="upload-xls">
                                                                XLS ექსპორტი
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-2 momd-rg-cl-no">
                                                        <div class="edit-delete-btns mom-del-btns">
                                                            <a href="#" class="editBTn">edit</a>
                                                            <a href="#" class="deleteBTn">delete</a>

                                                        </div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                        @endforeach
                                        <!-- end each order-->

                                    </div>
                                </div>
                                <!-- end panel each-->

                                <!-- panel each-->
                                <div role="tabpanel" class="tab-pane" id="toreceive-orders">

                                    <div class="tab-panel-inner">
                                        <div class="panel-header-filter">
                                            <div class="panel-header-filter-inner">
                                                <div class="order-check filter-check-all">
                                                    <div class="checkbox">
                                                        <label>
                                                                        <input type="checkbox" value="">
                                                                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                      </label>
                                                    </div>
                                                </div>
                                                <span class="filter-title">სორტირება:</span>
                                                <span class="sort-by-date active-filter">თარიღის შეცვლა</span>
                                                <span class="sort-by-name">RS.GE ატვირთვა</span>
                                                <span class="sort-by-name">ექსპორტირება</span>
                                                <div class="search_block-filtHead">
                                                    <div class="search-floating-bk">
                                                        <input type="text" style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--each order-->

                                        <!-- end each order-->

                                    </div>
                                </div>
                                <!-- end panel each-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end left

                <!-- right-->
                <div class="col-md-3 notifications-block">
                    <div class="main-right-block">
                        <div class="notifications-block">
                            <div class="notifications-block-inner">
                                <div class="notific-title">
                                    შეტყობინებები
                                </div>
                                <div class="notific-filter-head">
                                    <div class="notific-filter-head-inner">
                                        <!-- Nav tabs -->
                                        <ul class="tabsnavigation" role="tablist">
                                            <li role="presentation" class="active"><a href="#chatTab" aria-controls="chatTab" role="tab" data-toggle="tab">პრიორიტეტული</a></li>
                                            <li role="presentation"><a href="#notific-tab" aria-controls="notific-tab" role="tab" data-toggle="tab">ისტორია</a></li>
                                        </ul>
                                        <!-- end Nav tabs -->
                                    </div>
                                </div>
                                <div class="notifications-body">
                                    <!-- Tab panes -->
                                    <div class="tab-content user-page-tabContent">
                                        <!-- panel each-->
                                        <div role="tabpanel" class="tab-pane active" id="chatTab">

                                            <div class="tab-panel-block">
                                                <div class="tab-panel-inner">

                                                    <!--each order-->
                                                    <div class="each-order-block notification-each notification-each-active">
                                                        <div class="each-order-block-inner">
                                                            <div class="col-md-12">
                                                                <div class="notification-in-sm-bd">
                                                                    <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                    <div class="sub-message">
                                                                        ახალი შემოთავაზება
                                                                    </div>
                                                                    <div class="cls"></div>
                                                                </div>
                                                            </div>
                                                            <div class="cls"></div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <!-- end each order-->
                                                    <!--each order-->
                                                    <div class="each-order-block notification-each ">
                                                        <div class="each-order-block-inner">
                                                            <div class="col-md-12">
                                                                <div class="notification-in-sm-bd">
                                                                    <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                    <div class="sub-message">
                                                                        ახალი შემოთავაზება
                                                                    </div>
                                                                    <div class="cls"></div>
                                                                </div>
                                                            </div>
                                                            <div class="cls"></div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <!-- end each order-->
                                                    <!--each order-->
                                                    <div class="each-order-block notification-each ">
                                                        <div class="each-order-block-inner">
                                                            <div class="col-md-12">
                                                                <div class="notification-in-sm-bd">
                                                                    <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                    <div class="sub-message">
                                                                        ახალი შემოთავაზება
                                                                    </div>
                                                                    <div class="cls"></div>
                                                                </div>
                                                            </div>
                                                            <div class="cls"></div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <!-- end each order-->
                                                    <!--each order-->
                                                    <div class="each-order-block notification-each ">
                                                        <div class="each-order-block-inner">
                                                            <div class="col-md-12">
                                                                <div class="notification-in-sm-bd">
                                                                    <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                    <div class="sub-message">
                                                                        ახალი შემოთავაზება
                                                                    </div>
                                                                    <div class="cls"></div>
                                                                </div>
                                                            </div>
                                                            <div class="cls"></div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <!-- end each order-->

                                                </div>
                                            </div>
                                        </div>
                                        <!-- end panel each-->

                                        <!-- panel each-->
                                        <div role="tabpanel" class="tab-pane" id="notific-tab">

                                            <div class="tab-panel-inner">

                                                <!--each order-->
                                                <div class="each-order-block notification-each notification-each-active">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                <div class="sub-message">
                                                                    ახალი შემოთავაზება
                                                                </div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->

                                                <!--each order-->
                                                <div class="each-order-block notification-each notification-each-active">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                <div class="sub-message">
                                                                    ახალი შემოთავაზება
                                                                </div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->

                                                <!--each order-->
                                                <div class="each-order-block notification-each notification-each-active">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                <div class="sub-message">
                                                                    ახალი შემოთავაზება
                                                                </div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->

                                                <!--each order-->
                                                <div class="each-order-block notification-each notification-each-active">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor momwmauth">შპს “საქმშენი”</div>
                                                                <div class="sub-message">
                                                                    ახალი შემოთავაზება
                                                                </div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->

                                            </div>
                                        </div>
                                        <!-- end panel each-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <!--end  main top section -->

    <!-- main bottom section -->
    <section id="main-pg-bottom" class="section-block">
    <div class="main-section-in">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="main-left-block-bottom">
                        <div class="col-md-7 reporting-bot-bk">
                            <div class="receivedOrders-title">
                                რეპორტინგი
                            </div>
                            <div class="reporting-body">
                                <div class="reporting-body-inner">
                                    <img src="{{URL::asset('public/assets/img/reporting.png')}}" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 supplier-col">
                            <div class="receivedOrders-title">
                                შეკვეთები
                            </div>
                            <div class="supplier-body-bt">
                                <div class="supplier-body-bt-inner">
                                    <!--each-->
                                    @foreach ($promotions as $key => $promotion)
                                    <div class="momw-orders-sect">
                                        <div class="supplier-each active-supplier">
                                            <div class="supplier-each-inner">
                                                <div class="supplier-status @if($key !== 0) status-experienced @endif " >
                                                    @if($key === 0)
                                                        ახალი
                                                    @else
                                                        ძველი
                                                    @endif
                                                </div>
                                                <div class="supplier-title">
                                                    {{$promotion->production->organization["organization_name"]}}
                                                </div>
                                                <div class="supplier-imported-quantity">
                                                    {{count($promotion->sold_promotions)}}

                                                </div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        <div class="order-momw-subTxt">
                                            <div class="order-momw-subTxt-inner">
                                                {{$promotion->saleDetiles}}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- end each-->
                                    <div class="more-orders-supplier">
                                        <button>მეტი შეკვეთა</button>
                                    </div>
                                    <div class="cls"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 tender-bk-side">
                    <div class="receivedOrders-title">
                        ტენდერი
                    </div>
                    <div class="tender-body-side">
                        <div class="tender-body-side-inner">
                            <!--each-->
                            @foreach ($tenders as $key => $tender)
                                <div class="tender-each active-tender">
                                    <div class="tender-each-inner">
                                        <div class="tender-each-head">
                                            <div class="tender-each-head-in">
                                                {{$tender->productName}}
                                            </div>
                                        </div>
                                        <div class="tender-each-bottom">
                                            <div class="tender-each-bottom-in">
                                                <span class="tp-bg-med">წარმდგენი</span>
                                                <span class="tp-sm-md">{{$tender->user->organization->organization_name}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <!--end each-->

                            <div class="more-tender-btn">
                                <button>მეტი შეკვეთა</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </section>

@endsection
