@extends('layouts.master')
@section('content')
    <section id="user-information-section" class="section-block">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <!--left-->
                <div class="col-md-7">
                    <div class="row row-userPersonal">
                        <!--avatar-->
                        <div class="col-md-4">
                            <div class="user-avatar">
                                <img src="{{$user->organization->organization_picture[0]->image}}" alt="">
                            </div>
                        </div>
                        <!--end avatar-->
                        <!--user-personal info-->
                        <div class="col-md-4 col-userPersonal">
                            <div class="user-personal-info">
                                <div class="user-title">
                                    {{$user->first_name}} {{$user->last_name}}
                                </div>
                                <div class="user-type-comp">
                                    {{$user->organization->organization_type->organization_type}}

                                </div>
                                <div class="user-secondary-info">
                                    {{$user->organization->organization_slogan}}
                                </div>
                                <div class="user-additional-info">
                                    {{count($user->organization->production)}} პროდუქტი პლატფორმაზე
                                </div>
                            </div>
                        </div>
                        <!--end user-personal info-->
                        <!--contact info-->
                        <div class="col-md-4 col-userPersonal">
                            <div class="user-contact-ifno">
                                <div class="contact-title">
                                    საკონტაქტო ინფრომაცია:
                                </div>
                                <div class="contact-country">
                                    {{$user->organization->city->country->country}}
                                </div>
                                <div class="contact-city">
                                    {{$user->organization->city->city}}
                                </div>
                                <div class="contact-street">
                                    {{$user->organization->address}}
                                </div>
                                <div class="contact-zip-phone">
                                    <span class="zip-code">{{$user->organization->zip_code}}, </span>
                                    <span class="phone">  {{$user->organization->organization_phone->phone}}</span>
                                </div>
                            </div>
                        </div>
                        <!--contact info-->
                    </div>
                </div>
                <!--end left-->
                <!--right-->
                <div class="col-md-5 col-userPersonal">
                    <div class="button-block-def">
                        <button class="btn-writepers">პირადი მიმოწერა</button>
                    </div>
                </div>
                <!--end right-->
            </div>
        </div>
    </div>
    </section>
    <!--    orders section-->
  <section id="orders-section" class="section-block">
      <div class="section-block-inner">
          <div class="container">
              <div class="orders-tab-block">
                  <!-- Nav tabs -->
                  <ul class="tabsnavigation" role="tablist">
                      <li role="presentation" class="active"><a href="#received-orders" aria-controls="received-orders" role="tab" data-toggle="tab">მიღებული შეკვეთები</a></li>
                  </ul>
                  <!-- end Nav tabs -->
                  <!-- Tab panes -->
                  <div class="tab-content user-page-tabContent">
                      <!-- panel each-->
                      <div role="tabpanel" class="tab-pane active" id="received-orders">

                          <div class="tab-panel-block">
                              <div class="tab-panel-inner">
                                  <div class="panel-header-filter">
                                      <div class="panel-header-filter-inner">
                                          <span class="filter-title">სორტირება:</span>
                                          <span class="sort-by-date active-filter">თარიღის მიხედვით</span>
                                          <span class="sort-by-name">სახელის მიხედვით</span>
                                          <div class="search_block-filtHead">
                                              <div class="search-floating-bk">
                                                  <input type="text" style="display: none;">
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--each order-->
                                  @foreach($user->sold_production as $sold_production)
                                  <div class="each-order-block">
                                      <div class="each-order-block-inner">
                                          <div class="col-md-6">
                                              <div class="col-md-6">
                                                  <div class="order-icon-info-def">
                                                      <div class="order-icon">
                                                          <img src="{{$sold_production->production->image ? $sold_production->production->image : URL::asset('public/assets/img/order-icon.png')}}" alt="">
                                                      </div>
                                                      <div class="order-info-det">
                                                          <div class="order-title">
                                                            {{$sold_production->production->product_name}} <span>{{$sold_production->production->quantity}}{{$sold_production->production->quantity_format->quantityFormat}}</span>
                                                          </div>
                                                          <div class="order-date">
                                                              {{$sold_production->production->updated_at}}
                                                          </div>
                                                      </div>

                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="order-supplier">
                                                      <div class="top-txt">
                                                          მომწოდებელი:
                                                      </div>
                                                      <div class="bottom-txt">
                                                          {{$sold_production->production->organization->organization_name}}
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-3">
                                                  <div class="order-price">
                                                      <div class="top-txt">
                                                          ფასი:
                                                      </div>
                                                      <div class="bottom-txt">
                                                          {{$sold_production->price}}ლარი
                                                      </div>
                                                  </div>
                                              </div>

                                              <div class="col-md-3">
                                                  <div class="order-price">
                                                      <div class="top-txt">
                                                          გადახდის ტიპი:
                                                      </div>
                                                      <div class="bottom-txt">
                                                          {{$sold_production->paymentMethod}}
                                                      </div>
                                                  </div>
                                              </div>


                                          </div>
                                          <div class="col-md-6">
                                              <div class="order-more-btn">
                                                  <button type="button" class="btn-writepers">მეტი ინფორმაცია</button>
                                              </div>
                                          </div>
                                          <div class="cls"></div>
                                      </div>
                                      <div class="cls"></div>
                                  </div>
                                @endforeach
                                  <!-- end each order-->
                              </div>
                          </div>
                      </div>
                      <!-- end panel each-->
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--  end orders section-->
@endsection
