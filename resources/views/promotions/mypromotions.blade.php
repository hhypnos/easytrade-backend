@extends('layouts.master')
@section('content')
    <!--    orders section-->
<section id="orders-section-momw" class="section-block view-orders-section">
    <div class="section-block-inner">
        <div class="container">
            <!--  momwodebeli left-->
                <div class="col-md-9 momwodebeli-col">
                    <div class="momwodebeli-lefted">
                        <div class="orders-tab-block">
                            <!-- Nav tabs -->
                            <ul class="tabsnavigation" role="tablist">
                                <li role="presentation" class="active"><a href="#boughtaction" aria-controls="boughtaction" role="tab" data-toggle="tab">ჩემი შექმნილი აქციები
                                <span class="add-action"><img src="{{URL::to('public/assets/img/add.png')}}" alt=""></span></a></li>
                                <li role="presentation"><a href="#curentaction" aria-controls="curentaction" role="tab" data-toggle="tab">მიმდინარე აქციები</a></li>
                            </ul>
                        <!-- end Nav tabs -->
                        <!-- Tab panes -->
                        <div class="tab-content user-page-tabContent">
                            <!-- panel each-->
                            <div role="tabpanel" class="tab-pane active" id="boughtaction">

                                <div class="tab-panel-block">
                                    <div class="tab-panel-inner">
                                        <div class="panel-header-filter">
                                            <div class="panel-header-filter-inner">
                                                <span class="filter-title">აქციის ტიპი:</span>
                                                <span class="sort-by-date active-filter">ფასდაკლება</span>
                                                <span class="sort-by-name">ქეშბექი</span>
                                                <span class="sort-by-name">შეთავაზება</span>
                                                <div class="search_block-filtHead">
                                                    <div class="search-floating-bk">
                                                        <input type="text" style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--each order-->
                                        @foreach($promotions as $promotion)
                                        <div class="each-order-block" id="promotion-{{$promotion->id}}">
                                            <div class="each-order-block-inner">
                                                <div class="col-md-9 momdodeb-col-nop">
                                                    <div class="col-md-5">
                                                        <div class="order-icon-info-def">
                                                            <div class="order-icon">
                                                                <img src="{{$promotion->production->image ? $promotion->production->image : URL::asset('public/assets/img/order-icon.png')}}" alt="">
                                                            </div>
                                                            <div class="order-info-det">
                                                                <div class="order-title">
                                                                    {{$promotion->production->product_name}} <span>{{$promotion->production->quantity}}{{$promotion->production->quantity_format->quantityFormat}}</span>
                                                                </div>
                                                                <div class="order-date">
                                                                    {{$promotion->updated_at}}
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="order-supplier">
                                                            <div class="top-txt">
                                                                მომწოდებელი
                                                            </div>
                                                            <div class="bottom-txt">
                                                                {{$promotion->production->organization->organization_name}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-nopd">
                                                        <div class="order-price">
                                                            <div class="top-txt">
                                                                ფასი:
                                                            </div>
                                                            <div class="bottom-txt">
                                                                @if($promotion->production->sale =='yes')
                                                                    {{$promotion->production->price -(($promotion->production->price / 100) * $promotion->production->product_sale->newPrice)}}
                                                                @else
                                                                    {{$promotion->production->price}}
                                                                @endif
                                                                 ლარი
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="cls"></div>
                                                    <div class="col-pad-lf col-md-11">
                                                        <div class="sales-conditions">
                                                            <span class="title-cond">აქციის პირობები:</span>
                                                            <div class="sales-cond-body">
                                                                {{$promotion->description}}
                                                            </div>
                                                            <div class="sales-cond-percentage">
                                                                <span class="perc-title"> ფასდაკლების ოდენობა:</span>
                                                                <span class="perc-txt-bl">{{$promotion->saleDetiles}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 momd-rg-cl-no">
                                                  <div class="my-actions-edits">
                                                      <div class="edit-delete-btns">
                                                          <a href="#" class="editBTn">edit</a>
                                                          <a href="#" class="deleteBTn" onclick="deletePromotions({{$promotion->id}})">delete</a>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="cls"></div>
                                                <div class="cls"></div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                    @endforeach
                                        <!-- end each order-->

                                    </div>
                                </div>
                            </div>
                            <!-- end panel each-->

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function deletePromotions(item){
                         var CSRF_TOKEN = "{{csrf_token()}}";
                             $.ajax({
                                 /* the route pointing to the post function */
                                 url: '{{URL::to('deletepromotions')}}',
                                 type: 'POST',
                                 /* send the csrf-token and the input to the controller */
                                 data: {_token: CSRF_TOKEN, itemID:parseInt(item)},
                                 /* remind that 'data' is the response of the AjaxController */
                                 success: function (data) {
                                     if(data['error'] != undefined){
                                         return document.getElementById('error').innerHTML =
                                         '<div class="form-group "><div class="alert alert-danger"><ul><li>'+data['error']+'</li></ul></div> </div>';
                                    }
                                    // location.reload();
                                    $("#promotion-"+item).remove();
                                 }
                             });
                         }
            </script>
        </div>
    </div>
</section>
@endsection
