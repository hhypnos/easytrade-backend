@extends('layouts.master')
@section('content')
    <!--    orders section-->
    <section id="orders-section-momw" class="section-block view-orders-section">
    <div class="section-block-inner">
        <div class="container">
            <!--  momwodebeli left-->
            <div class="col-md-9 momwodebeli-col">
                <div class="momwodebeli-lefted">
                    <div class="orders-tab-block">
                        <!--tenderis damatebis bloki-->
                        <form method="post" action="{{URL::to('addtender')}}">
                            {{ csrf_field() }}
                        <div id="addTenderModal" class="addTenderModal modalEach">
                            <div class="addTenderModHeader">
                                ახალი ტენდერის დამატება
                            </div>
                            <div class="addTEnderModalInner">
                                <div class="addTenderModalBody">
                                    <div class="addTenderModalBody_inner">
                                        <!--each row-->
                                        <div class="eachModGrprow modalHalfCropped">
                                            <!--left-->
                                            <div class="leftedRowModal">
                                                <label>კომპანიის დასახელება:</label>
                                                <span class="companyNameFilled">
                                                {{auth()->user()->organization->organization_name}}
                                            </span>
                                            </div>
                                            <!-- end left-->

                                            <!--right-->
                                            <div class="rightedRowModal">
                                                <label for="productName">პროდუქციის დასახელება:</label>
                                                <input type="text" id="productName" name="productName">
                                            </div>
                                            <!-- endright-->
                                            <div class="cls"></div>
                                        </div>
                                        <!-- endeach row-->
                                        <!--each row-->
                                        <div class="eachModGrprow modalHalfCropped">
                                            <!--left-->
                                            <div class="leftedRowModal">
                                                <label for="TenderQuantity">ტენდერის ოდენობა:</label>
                                                <input type="text" id="TenderQuantity" name="quantity">
                                            </div>
                                            <!-- end left-->

                                            <!--right-->
                                            <div class="rightedRowModal">
                                                <div class="rightedCol-threehalfs">
                                                    <div class="twoOfThreeHafled-left">
                                                        <label for="TenderCost">ტენდერის ღირებულება:</label>
                                                        <input type="text" id="TenderCost" name="price">
                                                    </div>
                                                    <div class="twoOfThreeHafled-right">
                                                        <label for="tenderMeterDigit">ზომის ერთეული:</label>
                                                        <select name="quantityFormatID" id="tenderMeterDigit">
                                                          @foreach($quantityFormats as $quantityFormat)
                                                              <option value="{{$quantityFormat->quantityFormatID}}">{{$quantityFormat->quantityFormat}}</option>
                                                          @endforeach
                                                       </select>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                            </div>
                                            <!-- endright-->
                                            <div class="cls"></div>
                                        </div>
                                        <!-- endeach row-->
                                        <!--each row-->
                                        <div class="eachModGrprow modalHalfCropped">
                                            <!--left-->
                                            <div class="leftedRowModal">
                                                <label for="tenderStartDate">ტენდერის დაწყების თარიღი:</label>
                                                <input type="date" id="tenderStartDate" name="startTender">
                                            </div>
                                            <!-- end left-->

                                            <!--right-->
                                            <div class="rightedRowModal">
                                                <label for="tenderEndDAte">ტენდერის დასრულების თარიღი:</label>
                                                <input type="date" id="tenderEndDAte" name="endTender">
                                            </div>
                                            <!-- endright-->
                                            <div class="cls"></div>
                                        </div>
                                        <!-- endeach row-->
                                        <!--each row-->
                                        <div class="eachModGrprow modalHalfCropped">
                                            <div class="fullWidthRowMod">
                                                <label for="tenderRules">ტენდერის პირობები:</label>
                                                <textarea name="description" id="tenderRules"></textarea>
                                            </div>

                                            <div class="cls"></div>
                                        </div>
                                        <div class="eachModGrprow modalHalfCropped">
                                            <div class="fullWidthRowMod">
                                                <label for="tenderRules">ანგარიშსწორების პირობები:</label>
                                                <textarea name="paymentDescription" id="tenderRules"></textarea>
                                            </div>

                                            <div class="cls"></div>
                                        </div>
                                        <div class="eachModGrprow modalHalfCropped">
                                            <div class="fullWidthRowMod">
                                                <label for="tenderRules">პრეტენდენტის მიერ წარმოსადგენი დოკუმენტაცია:</label>
                                                <textarea name="candidateDescription" id="tenderRules"></textarea>
                                            </div>

                                            <div class="cls"></div>
                                        </div>
                                        <!-- endeach row-->
                                        <!--each row-->
                                        <div class="eachModGrprow modalHalfCropped">
                                            <div class="fullWidthRowMod">
                                                <div class="modalImportBtn">
                                                    <a href="#">იმპორტირება</a>
                                                </div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        <!-- endeach row-->

                                    </div>
                                </div>

                                <!--footer-->
                                <div class="addTenderModalFooter">
                                    <div class="modalFootLeft">
                                        <a href="#" class="addMoreTender">მეტი ტენდერის დამატება</a>
                                    </div>
                                    <div class="modalFootRight">
                                        <div class="TenderCanselBtn">
                                            <button onclick="hideModal()">გაუქმება</button>
                                        </div>
                                        <div class="TenderAddButton">
                                            <input type="submit" name="" value="დამატება" style="    border: none;
    background: #018bff;
    font-size: 12px;
    color: #fff;
    font-family: 'FiraGO-Medium';
    height: 100%;
    padding: 0px 52px;
    outline: 0;">
                                        </div>
                                    </div>
                                    <div class="cls"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end tenderis damatebis bloki-->
</form>
                        <!-- Nav tabs -->
                        <ul class="tabsnavigation" role="tablist">
                            <li role="presentation" class="active"><a href="#boughtaction" aria-controls="boughtaction" role="tab" data-toggle="tab">ჩემი შექმნილი აქციები
                            <span class="add-action" onclick="showModal()"><img src="img/add.png" alt=""></span></a></li>
                            <li role="presentation"><a href="#curentaction" aria-controls="curentaction" role="tab" data-toggle="tab">მიმდინარე აქციები</a></li>
                        </ul>
                        <!-- end Nav tabs -->
                        <!-- Tab panes -->
                        <div class="tab-content user-page-tabContent">
                            <!-- panel each-->
                            <div role="tabpanel" class="tab-pane active" id="boughtaction">

                                <div class="tab-panel-block">
                                    <div class="tab-panel-inner">
                                        <div class="panel-header-filter">
                                            <div class="panel-header-filter-inner">
                                                <span class="filter-title">აქციის ტიპი:</span>
                                                <span class="sort-by-date active-filter">ფასდაკლება</span>
                                                <span class="sort-by-name">ქეშბექი</span>
                                                <span class="sort-by-name">შეთავაზება</span>
                                                <div class="search_block-filtHead">
                                                    <div class="search-floating-bk">
                                                        <input type="text" style="display: none;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--each order-->
                                        <div class="each-order-block">
                                            <div class="each-order-block-inner">
                                                <div class="col-md-9 momdodeb-col-nop">
                                                    <div class="col-md-5">
                                                        <div class="order-icon-info-def">
                                                            <div class="order-icon">
                                                                <img src="img/order-icon.png" alt="">
                                                            </div>
                                                            <div class="order-info-det">
                                                                <div class="order-title">
                                                                    ზეთი ოლეინა <span>2ლ</span>
                                                                </div>
                                                                <div class="order-date">
                                                                    9 ივლისი, 17:00
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="order-supplier">
                                                            <div class="top-txt">
                                                                მომწოდებელი
                                                            </div>
                                                            <div class="bottom-txt">
                                                                კალვე
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-nopd">
                                                        <div class="order-price">
                                                            <div class="top-txt">
                                                                ფასი:
                                                            </div>
                                                            <div class="bottom-txt">
                                                                69 ლარი
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="cls"></div>
                                                    <div class="col-pad-lf col-md-11">
                                                        <div class="sales-conditions">
                                                            <span class="title-cond">აქციის პირობები:</span>
                                                            <div class="sales-cond-body">
                                                                ამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა
                                                            </div>
                                                            <div class="sales-cond-percentage">
                                                                <span class="perc-title"> ფასდაკლების ოდენობა:</span>
                                                                <span class="perc-txt-bl">- 60% საცალო შეფუთვისას</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 momd-rg-cl-no">
                                                    <div class="my-actions-edits">
                                                        <div class="edit-delete-btns">
                                                            <a href="#" class="editBTn">edit</a>
                                                            <a href="#" class="deleteBTn">delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        <!-- end each order-->

                                        <!--each order-->
                                        <div class="each-order-block">
                                            <div class="each-order-block-inner">
                                                <div class="col-md-9 momdodeb-col-nop">
                                                    <div class="col-md-5">
                                                        <div class="order-icon-info-def">
                                                            <div class="order-icon">
                                                                <img src="img/order-icon.png" alt="">
                                                            </div>
                                                            <div class="order-info-det">
                                                                <div class="order-title">
                                                                    ზეთი ოლეინა <span>2ლ</span>
                                                                </div>
                                                                <div class="order-date">
                                                                    9 ივლისი, 17:00
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="order-supplier">
                                                            <div class="top-txt">
                                                                მომწოდებელი
                                                            </div>
                                                            <div class="bottom-txt">
                                                                კალვე
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-nopd">
                                                        <div class="order-price">
                                                            <div class="top-txt">
                                                                ფასი:
                                                            </div>
                                                            <div class="bottom-txt">
                                                                69 ლარი
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="cls"></div>
                                                    <div class="col-pad-lf col-md-11">
                                                        <div class="sales-conditions">
                                                            <span class="title-cond">აქციის პირობები:</span>
                                                            <div class="sales-cond-body">
                                                                ამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა
                                                            </div>
                                                            <div class="sales-cond-percentage">
                                                                <span class="perc-title"> ფასდაკლების ოდენობა:</span>
                                                                <span class="perc-txt-bl">- 60% საცალო შეფუთვისას</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 momd-rg-cl-no">
                                                    <div class="my-actions-edits">
                                                        <div class="edit-delete-btns">
                                                            <a href="#" class="editBTn">edit</a>
                                                            <a href="#" class="deleteBTn">delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        <!-- end each order-->
                                    </div>
                                </div>
                            </div>
                            <!-- end panel each-->

                            <!-- panel each-->
                            <div role="tabpanel" class="tab-pane" id="curentaction">

                                <div class="tab-panel-inner">
                                    <div class="panel-header-filter">
                                        <div class="panel-header-filter-inner">
                                            <span class="filter-title">აქციის ტიპი:</span>
                                            <span class="sort-by-date active-filter">ფასდაკლება</span>
                                            <span class="sort-by-name">ქეშბექი</span>
                                            <span class="sort-by-name">შეთავაზება</span>
                                            <div class="search_block-filtHead">
                                                <div class="search-floating-bk">
                                                    <input type="text" style="display: none;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--each order-->
                                    <div class="each-order-block">
                                        <div class="each-order-block-inner">
                                            <div class="col-md-9 momdodeb-col-nop">
                                                <div class="col-md-5">
                                                    <div class="order-icon-info-def">
                                                        <div class="order-icon">
                                                            <img src="img/order-icon.png" alt="">
                                                        </div>
                                                        <div class="order-info-det">
                                                            <div class="order-title">
                                                                ზეთი ოლეინა <span>2ლ</span>
                                                            </div>
                                                            <div class="order-date">
                                                                9 ივლისი, 17:00
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <div class="order-supplier">
                                                        <div class="top-txt">
                                                            მომწოდებელი
                                                        </div>
                                                        <div class="bottom-txt">
                                                            კალვე
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-nopd">
                                                    <div class="order-price">
                                                        <div class="top-txt">
                                                            ფასი:
                                                        </div>
                                                        <div class="bottom-txt">
                                                            69 ლარი
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="cls"></div>
                                                <div class="col-pad-lf col-md-11">
                                                    <div class="sales-conditions">
                                                        <span class="title-cond">აქციის პირობები:</span>
                                                        <div class="sales-cond-body">
                                                            ამოყენებით ვღებულობთ იმაზე მეტ-ნაკლებად სწორი გადანაწილების ტექსტს, ვიდრე გამეორებადი სიტყვებია ხოლმე. შედეგად, ტექსტი ჩვეულებრივ ინგლისურს გავს, მისი შეუძლებელია. დღეს უამრავი პერსონალური საგამომცემლო პროგრამა
                                                        </div>
                                                        <div class="sales-cond-percentage">
                                                            <span class="perc-title"> ფასდაკლების ოდენობა:</span>
                                                            <span class="perc-txt-bl">- 60% საცალო შეფუთვისას</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 momd-rg-cl-no">
                                                <div class="my-actions-edits">
                                                    <div class="edit-delete-btns">
                                                        <a href="#" class="editBTn">edit</a>
                                                        <a href="#" class="deleteBTn">delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cls"></div>
                                        </div>
                                        <div class="cls"></div>
                                    </div>
                                    <!-- end each order-->

                                </div>
                            </div>
                            <!-- end panel each-->
                        </div>
                    </div>
                </div>
            </div>
            <!--  end momwodebeli left -->

            <!--cart-->

            <div class="col-md-3 notifications-block">
                <div class="main-right-block">
                    <div class="notifications-block">
                        <div class="notifications-block-inner">
                            <div class="notific-title cart-title">
                                მიმოწერა
                            </div>
                            <div class="notific-filter-head">
                                <div class="notific-filter-head-inner">
                                    <!-- Nav tabs -->
                                    <ul class="tabsnavigation" role="tablist">
                                        <li role="presentation" class="active"><a href="#chatTab" aria-controls="chatTab" role="tab" data-toggle="tab">ჩატი</a></li>
                                        <li role="presentation"><a href="#notific-tab" aria-controls="notific-tab" role="tab" data-toggle="tab">ნოტიფიკაციები</a></li>
                                    </ul>
                                    <!-- end Nav tabs -->
                                </div>
                            </div>
                            <div class="notifications-body">
                                <!-- Tab panes -->
                                <div class="tab-content user-page-tabContent">
                                    <!-- panel each-->
                                    <div role="tabpanel" class="tab-pane active" id="chatTab">

                                        <div class="tab-panel-block">
                                            <div class="tab-panel-inner">

                                                <!--each order-->
                                                <div class="each-order-block notification-each notification-each-active">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor">შპს “საქმშენი”</div>
                                                                <div class="message-quantity">6</div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->
                                                <!--each order-->
                                                <div class="each-order-block notification-each ">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor">შპს “საქმშენი”</div>
                                                                <div class="message-quantity">6</div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->
                                                <!--each order-->
                                                <div class="each-order-block notification-each ">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor">შპს “საქმშენი”</div>
                                                                <div class="message-quantity">6</div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->
                                                <!--each order-->
                                                <div class="each-order-block notification-each ">
                                                    <div class="each-order-block-inner">
                                                        <div class="col-md-12">
                                                            <div class="notification-in-sm-bd">
                                                                <div class="messageAuthor">შპს “საქმშენი”</div>
                                                                <div class="message-quantity">6</div>
                                                                <div class="cls"></div>
                                                            </div>
                                                        </div>
                                                        <div class="cls"></div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <!-- end each order-->

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end panel each-->

                                    <!-- panel each-->
                                    <div role="tabpanel" class="tab-pane" id="notific-tab">

                                        <div class="tab-panel-inner">

                                            <!--each order-->
                                            <div class="each-order-block notification-each notification-each-active">
                                                <div class="each-order-block-inner">
                                                    <div class="col-md-12">
                                                        <div class="notification-in-sm-bd">
                                                            <div class="messageAuthor">შპს “საქმშენი”</div>
                                                            <div class="message-quantity">6</div>
                                                            <div class="cls"></div>
                                                        </div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <!-- end each order-->

                                            <!--each order-->
                                            <div class="each-order-block notification-each notification-each-active">
                                                <div class="each-order-block-inner">
                                                    <div class="col-md-12">
                                                        <div class="notification-in-sm-bd">
                                                            <div class="messageAuthor">შპს “საქმშენი”</div>
                                                            <div class="message-quantity">6</div>
                                                            <div class="cls"></div>
                                                        </div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <!-- end each order-->

                                            <!--each order-->
                                            <div class="each-order-block notification-each notification-each-active">
                                                <div class="each-order-block-inner">
                                                    <div class="col-md-12">
                                                        <div class="notification-in-sm-bd">
                                                            <div class="messageAuthor">შპს “საქმშენი”</div>
                                                            <div class="message-quantity">6</div>
                                                            <div class="cls"></div>
                                                        </div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <!-- end each order-->

                                            <!--each order-->
                                            <div class="each-order-block notification-each notification-each-active">
                                                <div class="each-order-block-inner">
                                                    <div class="col-md-12">
                                                        <div class="notification-in-sm-bd">
                                                            <div class="messageAuthor">შპს “საქმშენი”</div>
                                                            <div class="message-quantity">6</div>
                                                            <div class="cls"></div>
                                                        </div>
                                                    </div>
                                                    <div class="cls"></div>
                                                </div>
                                                <div class="cls"></div>
                                            </div>
                                            <!-- end each order-->

                                        </div>
                                    </div>
                                    <!-- end panel each-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <!--  end orders section-->
  <!--  end orders section-->

@endsection
