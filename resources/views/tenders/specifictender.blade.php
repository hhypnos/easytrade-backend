@extends('layouts.master')
@section('content')
    <div class="container">
    <div class="back-button">
        უკან დაბრუნება
    </div>
    <div class="order-detail-bk tenderSendingBlock">
        <div class="order-det-head">
            <div class="order-det-head-inner">
                <div class="tender-sendingHeadtxt">
                    ტენდერი საოფისე ტექნიკის შესყიდვაზე
                </div>
            </div>
        </div>
        <div class="order-detail-bk-in">
            <div class="row">
                <div class="col-md-7">
                    <div class="top-row">
                        <div class="col-md-4">
                            <div class="ord-tp-tit">
                                წარმდგენი
                            </div>
                            <div class="ord-tp-bt">
                                {{$tender->user->organization->organization_name}}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="ord-tp-tit">
                                რაოდენობა
                            </div>
                            <div class="ord-tp-bt">
                                <span class="txt-blue">{{$tender->quantity}} {{$tender->quantity_format->quantityFormat}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="ord-tp-tit">
                                სასურველი ფასი
                            </div>
                            <div class="ord-tp-bt">
                                {{$tender->price}} ლარი
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="ord-tp-tit">
                                ვადა
                            </div>
                            <div class="ord-tp-bt">
                                {{$tender->startTender}} {{$tender->endTender}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tendetSendingdesc">
                                <div class="ord-tp-tit">
                                    ტენდერის აღწერილობა
                                </div>
                                <div class="tenderSendescParag">
                                    {{$tender->description}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tendetSendingdesc">
                                <div class="ord-tp-tit">
                                    ანგარიშსწორების პირობები:
                                </div>
                                <div class="tenderSendescParag">
                                    {{$tender->paymentDescription}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tendetSendingdesc">
                                <div class="tenderSendescParag">
                                    {{$tender->candidateDescription}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tendetSendingdesc">
                                <div class="ord-tp-tit">
                                    პრეტენდენტის მიერ წარმოსადგენი დოკუმენტაცია:
                                </div>
                                <div class="tenderSendescParag">
                                    ფასების ცხრილი (სატენდერო დოკუმენტაციის მე-7 მუხლი, ფურცელი #9); კომპანიის რეკვიზიტები; MAF- მწარმოებლის ავტორიზაციის ფორმა; საქართველოში არსებული მინიმუმ ორი სერვის ცენტრის მისამართი და საკონტაქტო ნომერი (მისამართები დამკვეთის მიერ, გადამოწმდება ადგილზე).
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tendetSendingdesc">
                                <div class="ord-tp-tit">
                                    დოკუმენტები
                                </div>
                                <div class="documentsButtonsBot">
                                    <div class="eachBTnDocs">
                                        <a href="{{$tender->file}}">ხელშეკრულება.docx</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <div class="takePartBTn">
                        <button>მონაწილეობის მიღება</button>
                    </div>
                    <div class="cls"></div>
                    <div class="tenderSending-DeTprof">
                        <div class="headOfdetprof">
                            <img src="{{URL::asset('public/assets/img/detprofhead.png')}}" alt="">
                            <span class="profileavatrABs">
                                <img src="{{$tender->user->organization->organization_picture[0]->image}}" alt="">
                            </span>
                        </div>
                        <div class="detprofBody">
                            <div class="detprof-namecomp">
                                {{$tender->user->organization->organization_name}}
                            </div>
                            <div class="detprof-catcomp">
                                {{$tender->user->organization->organization_type->organization_type}}
                            </div>
                            <div class="detprof-yearsoftrade">
                                {{$tender->user->organization->organization_slogan}}
                            </div>
                            <div class="detprof-prodnumber">
                                {{count($tender->user->organization->production)}} პროდუქტი პლატფორმაზე
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
