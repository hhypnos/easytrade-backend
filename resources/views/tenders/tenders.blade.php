@extends('layouts.master')
@section('content')
    <div class="container">
    <div class="tender-list-block">
        <div class="tender-list-headers">
            <ul class="tabsnavigation" role="tablist">
                <li role="presentation" class="active"><a href="#myTdenders" aria-controls="myTdenders" role="tab" data-toggle="tab">ჩემი ტენდერები</a></li>
                <li role="presentation"><a href="#currentTender" aria-controls="currentTender" role="tab" data-toggle="tab">მიმდინარე ტენდერები</a></li>
            </ul>
        </div>
        <div class="tender-list-body">
            <div class="tender-list-sortHeader">

            </div>
            <div class="tender-list-body-inner">

                <div class="tab-content user-page-tabContent">
                    <!-- panel each-->
                    <div role="tabpanel" class="tab-pane active" id="myTdenders">

                        <div class="tab-panel-block">
                            <div class="tab-panel-inner">
                                <div class="panel-header-filter filter-head-tender">
                                    <div class="panel-header-filter-inner">
                                        <span class="filter-title">სორტირება:</span>
                                        <span class="sort-by-date active-filter">თარიღის მიხედვით</span>
                                        <span class="sort-by-name">სახელის მიხედვით</span>
                                        <div class="search_block-filtHead">
                                            <div class="search-floating-bk">
                                                <input type="text" style="display: none;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--each order-->
                                @foreach ($tenders as $tender)
                                    @if($tender->userID == auth()->user()->id)
                                <div class="tender-list-each-item">
                                    <div class="tender-list-each-item-inner">
                                        <div class="list-item-head">
                                            <div class="list-item-head-in">
                                                <div class="tender-title">
                                                    {{$tender->productName}}
                                                </div>
                                                <div class="tender-takePartBtn">
                                                    <a href="{{URL::to('tender/'.$tender->id)}}"><button>მონაწილეობის მიღება</button></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tender-item-bottom">
                                            <div class="tender-item-bottom-in">
                                                <div class="col-md-7">
                                                    <div class="col-md-4">
                                                        <div class="each-col-inner-tender">
                                                            <div class="txt-top-tender">
                                                                წარმდგენი
                                                            </div>
                                                            <div class="txt-bot-tender">
                                                                {{$tender->user->organization->organization_name}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="each-col-inner-tender">
                                                            <div class="txt-top-tender">
                                                                რაოდენობა
                                                            </div>
                                                            <div class="txt-bot-tender">
                                                                <span class="txt-bot-blue">{{$tender->quantity}} {{$tender->quantity_format->quantityFormat}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="each-col-inner-tender">
                                                            <div class="txt-top-tender">
                                                                სასურველი ფასი
                                                            </div>
                                                            <div class="txt-bot-tender">
                                                                {{$tender->price}} ლარი
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="col-md-4">
                                                        <div class="each-col-inner-tender tend-righted">
                                                            <div class="txt-top-tender">
                                                                ვადა
                                                            </div>
                                                            <div class="txt-bot-tender">
                                                                {{$tender->startTender}} {{$tender->endTender}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                <!-- end each order-->




                            </div>
                        </div>
                    </div>
                    <!-- end panel each-->

                    <!-- panel each-->
                    <div role="tabpanel" class="tab-pane" id="currentTender">

                        <div class="tab-panel-inner">
                            <div class="panel-header-filter filter-head-tender">
                                <div class="panel-header-filter-inner">
                                    <span class="filter-title">სორტირება:</span>
                                    <span class="sort-by-date">თარიღის მიხედვით</span>
                                    <span class="sort-by-name">სახელის მიხედვით</span>
                                    <div class="search_block-filtHead">
                                        <div class="search-floating-bk">
                                            <input type="text" style="display: none;">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--each order-->
                            @foreach ($tenders as $tender)
                            <div class="tender-list-each-item">
                                <div class="tender-list-each-item-inner">
                                    <div class="list-item-head">
                                        <div class="list-item-head-in">
                                            <div class="tender-title">
                                                {{$tender->productName}}
                                            </div>
                                            <div class="tender-takePartBtn">
                                                <a href="{{URL::to('tender/'.$tender->id)}}"><button>მონაწილეობის მიღება</button></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tender-item-bottom">
                                        <div class="tender-item-bottom-in">
                                            <div class="col-md-7">
                                                <div class="col-md-4">
                                                    <div class="each-col-inner-tender">
                                                        <div class="txt-top-tender">
                                                            წარმდგენი
                                                        </div>
                                                        <div class="txt-bot-tender">
                                                            {{$tender->user->organization->organization_name}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="each-col-inner-tender">
                                                        <div class="txt-top-tender">
                                                            რაოდენობა
                                                        </div>
                                                        <div class="txt-bot-tender">
                                                            <span class="txt-bot-blue">{{$tender->quantity}} {{$tender->quantity_format->quantityFormat}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="each-col-inner-tender">
                                                        <div class="txt-top-tender">
                                                            სასურველი ფასი
                                                        </div>
                                                        <div class="txt-bot-tender">
                                                            {{$tender->price}} ლარი
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="col-md-4">
                                                    <div class="each-col-inner-tender tend-righted">
                                                        <div class="txt-top-tender">
                                                            ვადა
                                                        </div>
                                                        <div class="txt-bot-tender">
                                                            {{$tender->startTender}} {{$tender->endTender}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- end each order-->

                        </div>
                    </div>
                    <!-- end panel each-->
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
