<!-- header-->
<header>
  <div class="header-inner">
      <div class="container">
          <div class="row">
              <div class="col-md-3">
                  <div class="head-logo-block">
                      <img src="{{URL::asset('public/assets/img/logo.png')}}" class="img-responsive" alt="LOGO">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="search-block-head">
                      <div class="search-group">
                          <input type="text" placeholder="საძიებო სიტყვა">
                          <button><i class="glyphicon glyphicon-search"></i></button>
                      </div>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="header-user-panel">
                      <!-- Single button -->
                      <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">გამარჯობა, {{auth()->user()->first_name}}<span class="caret"></span></button>
                          <ul class="dropdown-menu">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="{{URL::to('logout')}}">გასვლა</a></li>
                          </ul>
                      </div>
                  </div>
              </div>
          </div>

      </div>
      <div class="cls"></div>
  </div>
</header>
<!-- end header-->
