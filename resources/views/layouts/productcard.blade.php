<!--cart-->
<div class="col-md-3 cart-col">
    <div class="cart-right">

        <div class="cart-title">
            მიმოწერა
        </div>
        <div class="notific-filter-head">
            <div class="notific-filter-head-inner">
                <!-- Nav tabs -->
                <ul class="tabsnavigation" role="tablist">
                    <li role="presentation" class="active"><a href="#current-session" aria-controls="current-session" role="tab" data-toggle="tab">ამჟამინდელი სესია</a></li>
                    <li role="presentation"><a href="#saved-session" aria-controls="notific-tab" role="tab" data-toggle="tab">გადანახული</a></li>
                </ul>
                <!-- end Nav tabs -->
            </div>
        </div>
        <div class="notifications-body">
            <!-- Tab panes -->
            <div class="tab-content user-page-tabContent">
                <!-- panel each-->

                <div role="tabpanel" class="tab-pane active" id="current-session">

                    <div class="tab-panel-block">
                        <div class="tab-panel-inner" id="products">

                            <!--each order-->
                            {{-- {{dd(Session::get('items.products'))}} --}}
                            @if(Session::has('items.products'))
                                @foreach(Session::get('items.products') as $key => $card)
                            <div class="cart-each">
                                <div class="cart-each-inner">
                                    <div class="cart-item-title">
                                        {{\App\Production::find($card)->product_name}}
                                    </div>
                                    <div class="cart-item-quantity">
                                        <span class="quantity-txt">რაოდენობა</span>
                                        <span class="quantity-digit">
                                            <input type="number" class="quantity" name="quantity" value="{{\App\Production::find($card)->quantity}}" style="width:31px">
                                            {{\App\Production::find($card)->quantity_format->quantityFormat}}
                                        </span>
                                        <span class="spinners"></span>
                                    </div>
                                    <div class="cart-item-price">
                                        <span>ფასი:</span>
                                        <span class="price-green">
                                            @if(\App\Production::find($card)->sale =='yes')
                                                {{\App\Production::find($card)->price -((\App\Production::find($card)->price / 100) * \App\Production::find($card)->product_sale->newPrice)}}
                                            @else
                                                {{\App\Production::find($card)->price}}
                                            @endif
                                            ლარი</span>
                                    </div>
                                    <div class="cart-item-quantity">
                                        <span>მიწოდების თარიღი:</span>
                                        <input type="date" class="orderDate" name="orderDate" >
                                    </div>
                                    <div class="cart-item-payment">
                                        <span>გადარიცხვის მეთოდი:</span>
                                        <select class="paymentMethod" name="paymentMethod">
                                            <option value="cash">cash</option>
                                            <option value="transaction">transaction</option>
                                            <option value="consignation">consignation</option>
                                        </select>

                                    </div>
                                    <br>
                                    <div class="save-for-later">
                                        <a href="#">გადანახვა</a>

                                    </div>
                                    <div class="delete-cart-item">
                                        <a href="#" onclick="deleteFromCard({{$key}},this)">delete</a>
                                    </div>
                                </div>
                                </div>
                                @endforeach
                            @endif
                                <!-- end each order-->

                        </div>
                    </div>
                    <!-- end panel each-->

                    <div class="sent-order-btn">
                        <button onclick="buyItemsFromCard()">შეკვეთის გაგზავნა</button>
                    </div>
                    <div class="errors" id="error">
                        @include('layouts.errors')
                    </div>
                </div>

                <!-- panel each-->
                <div role="tabpanel" class="tab-pane" id="saved-session">

                    <div class="tab-panel-inner">





                    </div>
                </div>
                <!-- end panel each-->

            </div>
        </div>
    </div>
    <!-- end cart-->
    <script type="text/javascript">
        function addToCard(item){

                 var CSRF_TOKEN = "{{csrf_token()}}";
                     $.ajax({
                         /* the route pointing to the post function */
                         url: '{{URL::to('addtocard')}}',
                         type: 'POST',
                         /* send the csrf-token and the input to the controller */
                         data: {_token: CSRF_TOKEN, itemID:item},
                         /* remind that 'data' is the response of the AjaxController */
                         success: function (data) {
                             if(data['error'] != undefined){
                                 return document.getElementById('error').innerHTML =
                                 '<div class="form-group "><div class="alert alert-danger"><ul><li>'+data['error']+'</li></ul></div> </div>';
                            }
                            // location.reload();
                            $( "#products" ).load(window.location.href + " #products" );
                         }
                     });
                 }

                 function deleteFromCard(item,element){
                     // event.prevendDefault();
                          var CSRF_TOKEN = "{{csrf_token()}}";
                              $.ajax({
                                  /* the route pointing to the post function */
                                  url: '{{URL::to('deletefromcard')}}',
                                  type: 'POST',
                                  /* send the csrf-token and the input to the controller */
                                  data: {_token: CSRF_TOKEN, itemKey:item},
                                  /* remind that 'data' is the response of the AjaxController */
                                  success: function (data) {
                                      if(data['error'] != undefined){
                                          return document.getElementById('error').innerHTML =
                                          '<div class="form-group "><div class="alert alert-danger"><ul><li>'+data['error']+'</li></ul></div> </div>';
                                     }
                                     element.parentNode.parentNode.parentNode.remove();
                                  }
                              });
                          }
                function buyItemsFromCard(){
                    let quantity = [];
                    let orderDate = [];
                    let paymentMethod = [];
                    for (var i = 0; i < document.getElementsByClassName('quantity').length; i++) {
                        quantity[i] = document.getElementsByClassName('quantity')[i].value;
                    }
                    for (var i = 0; i < document.getElementsByClassName('orderDate').length; i++) {
                        orderDate[i] = document.getElementsByClassName('orderDate')[i].value;
                    }
                    for (var i = 0; i < document.getElementsByClassName('paymentMethod').length; i++) {
                        paymentMethod[i] = document.getElementsByClassName('paymentMethod')[i].value;
                    }
                    if(quantity.length > 0){
                        // console.log(orderDate);
                  // event.prevendDefault();
                       var CSRF_TOKEN = "{{csrf_token()}}";
                           $.ajax({
                               /* the route pointing to the post function */

                               url: '{{URL::to('buyitemsfromcard')}}',
                               type: 'POST',
                               /* send the csrf-token and the input to the controller */
                               data: {_token: CSRF_TOKEN, itemQuantity:quantity,itemOrderDate:orderDate,itemPaymentMethod:paymentMethod},
                               /* remind that 'data' is the response of the AjaxController */
                               success: function (data) {
                                   if(data['error'] != undefined){
                                       return document.getElementById('error').innerHTML =
                                       '<div class="form-group "><div class="alert alert-danger"><ul><li>'+data['error']+'</li></ul></div> </div>';
                                  }
                                  location.reload();
                              }
                           });
                       }
                       }
    </script>
