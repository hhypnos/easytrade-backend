@extends('layouts.master')

@section('content')
    <div class="login-page">
        <div class="login-page-inner">
            <div class="left-login-block-outher">
                <div class="left-login-block">
                    <div class="top-logo">
                        <img src="{{URL::asset('public/assets/img/logo.png')}}" alt="">
                    </div>
                    <div class="login-block">
                        <div class="pg-title reg-title">
                            რეგისტრაცია
                        </div>
                        <form method="post" action="{{URL::to('signup')}}">
                            {{ csrf_field() }}
                        <div class="form-group-outher">
                            <div class="form-group float-label-control">
                                <label for="username">მომხმარებლის სახელი</label>
                                <input type="text" class="form-control" id="username" name="su">
                            </div>
                            <div class="form-group float-label-control">
                                <label for="password">პაროლი</label>
                                <input type="password" class="form-control" id="password" name="sp">
                            </div>
                            <div class="form-group float-label-control">
                                <label for="tin">საიდენტიფიკაციო კოდი</label>
                                <input type="text" class="form-control" id="tin" name="tin">
                            </div>
                            <div class="form-group float-label-control">
                                <select name="userOrCompany" id="category-type">
                                 <option value="0" selected >კატეგორია</option>
                                    <option value="1">მომხმარებელი</option>
                                  <option value="2">მომწოდებელი</option>
                              </select>
                            </div>
                            <div class="cls"></div>
                            <div class="buttons-group">
                                <div class="btn-grp-each register-btn">
                                    <input type="submit" class="btn btn-default authentificationButton" value="რეგისტრაცია">
                                </div>
                                <div class="reg-auth-txt">
                                    ხართ უკვე რეგისტრირებული?
                                    <br>
                                    <a href="{{URL::to('login')}}">ავტორიზაცია</a>
                                </div>
                            </div>
                        </div>
                        @include('layouts.errors')
                    </form>
                    </div>
                </div>
            </div>
            <div class="right-login-block">
                <div class="bg-half-img">
                    <img src="{{URL::asset('public/assets/img/bg-half.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection
