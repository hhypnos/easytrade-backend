@extends('layouts.master')

@section('content')
    <div class="login-page">
        <div class="login-page-inner">
            <div class="left-login-block-outher">
                <div class="left-login-block">
                    <div class="top-logo">
                        <img src="{{URL::asset('public/assets/img/logo.png')}}" alt="">
                    </div>
                    <div class="login-block">
                        <div class="pg-title">
                            სისტემაში ავტორიზაცია
                        </div>
                        <div class="project-desc">
                            Easy Trade არის თანამედროვე პლატფორმა რომელიც აადვილებს დისტრიბუტორების და მომწოდებლების ურთიერთობას და ასევე ეხმარება მათ ეფექტურად აწარმოონ ფინანსური მოქმედებები
                        </div>
                        <form method="post" action="{{URL::to('signin')}}">
                            {{ csrf_field() }}
                        <div class="form-group-outher">
                            <div class="form-group float-label-control">
                                <label for="username">მომხმარებლის სახელი</label>
                                <input type="text" class="form-control" id="username" name="username">
                            </div>
                            <div class="form-group float-label-control">
                                <label for="password">პაროლი</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="cls"></div>
                            <div class="buttons-group">
                                <div class="btn-grp-each auth-btn">
                                    <input type="submit" class="btn btn-primary loginButton" value="ავტორიზაცია">
                                </div>

                                <div class="btn-grp-each reg-btn">
                                    <a href="{{URL::to('registration')}}" class="btn btn-default registerButton">რეგისტრაცია</a>
                                </div>
                            </div>
                        </div>
                        @include('layouts.errors')
                    </form>
                    </div>
                </div>
            </div>
            <div class="right-login-block">
                <div class="bg-half-img">
                    <img src="{{URL::asset('public/assets/img/bg-half.png')}}" alt="">
                </div>
            </div>
            <div class="cls"></div>
        </div>
    </div>

@endsection
