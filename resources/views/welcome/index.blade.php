<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Easy trade</title>
    <!-- Meta Share -->
    <meta property="og:title" content="Start.ly — Agency One Page Parallax Template" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{URL::asset('public/assets/main_view/images/screen.jpg')}}" />
    <!-- CSS Files -->
    <link href="https://fonts.googleapis.com/css?family=Product+Sans:300,400,700" rel="stylesheet">
    <!-- build:css css/app.min.css -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{URL::asset('public/assets/main_view/css/global/bootstrap.min.css')}}">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{URL::asset('public/assets/main_view/css/global/plugins/icon-font.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{URL::asset('public/assets/main_view/css/style.css')}}">
    <!-- /build -->
</head>

<body class="overflow-hidden">
    <header id="home">

        <!-- navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <h3 class="gradient-mask">Easy Trade</h3>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#site-nav" aria-controls="site-nav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

                <div class="collapse navbar-collapse" id="site-nav">
                    <ul class="navbar-nav text-sm-left ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#features">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#pricing">Pricing</a>
                        </li>
<!--
                        <li class="nav-item">
                            <a class="nav-link" href="blog.html">Blog</a>
                        </li>
-->

<!--
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown">Pages <span class="pe-2x pe-7s-angle-down"></span>  </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="index-two.html">Landing Style Two</a>
                                <a class="dropdown-item" href="blog.html">Blog Page</a>
                                <a class="dropdown-item" href="blog-single.html">Blog Single</a>
                            </div>
                        </li>
-->
                        <li class="nav-item">
                            <a class="nav-link" href="#footer">Help</a>
                        </li>
                        @auth
                            <li class="nav-item text-center">
                                <a href="#signup" class="btn align-middle btn-outline-primary my-2 my-lg-0">{{auth()->user()->rs_su}}</a>
                            </li>
                        @else
                        <li class="nav-item text-center">
                            <a href="{{URL::to('login')}}" class="btn align-middle btn-outline-primary my-2 my-lg-0">Login</a>
                        </li>
                        <li class="nav-item text-center">
                            <a href="{{URL::to('registration')}}" class="btn align-middle btn-primary my-2 my-lg-0">Sign Up</a>
                        </li>
                        @endauth
                    </ul>

                </div>
            </div>
        </nav>
        <!-- // end navbar -->


        <!-- hero -->
        <section class="jumbotron-two">

            <div class="container">

                <div class="row align-items-center">
                    <div class="col-12 col-md-5">

                        <h1 class="display-5">New Marketplace New Opportunities.</h1>
                        <p class="text-muted mb-3">B2B cloud based e-commerce platform that suits your needs of business management</p>
                        <p>
                            <a href="#signup" class="btn btn-xl btn-primary">Get started free</a>
                        </p>

                    </div>
                    <div class="col-12 col-md-7 my-3 my-md-lg">

                        <div class="youtube cast-shadow" data-video-id="NkDKJ43DEL8" data-params="modestbranding=1&amp;showinfo=0&amp;controls=1&amp;vq=hd720">
                            <img src="{{URL::asset('public/assets/main_view/images/1.jpg')}}" alt="image" class="img-fluid">
                            <div class="play"><span class="pe-7s-play pe-3x"></span></div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
        <!-- // end hero -->


        <div class="bg-shape"></div>
        <div class="bg-circle"></div>
        <div class="bg-circle-two"></div>

    </header>
    <div class="section" id="intro">
        <div class="container">

            <div class="client-logos text-center">
                <p class="text-muted">TRUSTED BY MOST POPULAR BRANDS</p>

                <img src="{{URL::asset('public/assets/main_view/images/client_logo_1.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_2.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_3.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_4.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_5.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_6.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_7.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_8.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_9.png')}}" alt="client logo" />
                <img src="{{URL::asset('public/assets/main_view/images/client_logo_10.png')}}" alt="client logo" />

            </div>
            <!-- // end .client-logos -->

        </div>
        <!-- // end .container -->
    </div>
    <!-- // end #services.section -->
    <div class="section bg-light pt-lg">
        <div class="container">

            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-plugin pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Easy To Use</h5> Upload full assortment of your product, manage orders, inventory and logistics
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-rocket pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Fast Operations</h5> Make B2B operations in a moment. Negotiations Orders Contracts
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-piggy pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Save Money</h5> Save time, Manage resources. Discover new market without additional costs
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-cloud-upload pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Cloud Based</h5> Sync to cloud, never lose data. Real time process management
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-config pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Easy Integratable</h5> Integrated with accounting and logistics softwares
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-display1 pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Reports and Analytics</h5> Get important reports for set period of time. analyze your daily routine
                        </div>
                    </div>
                </div>
                <!-- // end .col -->
            </div>
        </div>
    </div>

    <!-- Features -->
    <div class="section" id="features">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-8">
                    <div class="browser-window limit-height my-5 mr-0 mr-sm-5">
                        <div class="top-bar">
                            <div class="circles">
                                <div class="circle circle-red"></div>
                                <div class="circle circle-yellow"></div>
                                <div class="circle circle-blue"></div>
                            </div>
                        </div>
                        <div class="content">
                            <img src="{{URL::asset('public/assets/main_view/images/dashboard.jpg')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="media">
                        <div class="media-body">
                            <div class="media-icon mb-3"> <i class="pe-7s-airplay pe-3x"></i> </div>
                            <h3 class="mt-0">Perfect Dashboard</h3>
                            <p> Dashboard gives gives all necessary KPIs to monitor the health of a business, with its easy assembly menu blocks. It gives opportunity to manage and take actions in different processes like ordering, negotiations, online contracts, accounting system, logistics and warehouse management. All the menu blocks can be expanded to see detailed information or to take action. Platform is supported to integrate with different IOT and software systems for better process management which leads to transparent system in your business.</p>
                        </div>
                    </div>
                </div>

            </div>



            <div class="row align-items-center mt-5">

                <div class="col-sm-4">
                    <div class="media">
                        <div class="media-body">
                            <div class="media-icon mb-3"> <i class="pe-7s-graph1 pe-3x"></i> </div>
                            <h3 class="mt-0">Reports, Analytics & more</h3>
                            <p> Getting reports making analytics and managing future action never been so easy. platform gives all the necessary information with smart charts and our UX design provides pleasure interacting with interface by its Simplicity. artificial intelligence part of platform gives you smart analytics for everyday business management and helps to plan future actions. With help of beyond mentioned AI, Software will Generate notifiations about your current situation and upcoming events, risks for preventing future problems. All of this tools are built for increasing efficiency.
</p>
                        </div>
                    </div>
                </div>


                <div class="col-sm-8">
                    <img src="{{URL::asset('public/assets/main_view/images/screen.jpg')}}" alt="image" class="img-fluid cast-shadow my-5">
                </div>


            </div>
        </div>



    </div>



    <!-- features -->


    <div class="section bg-light py-lg">
        <div class="container">

            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="media">

                        <!-- // end .di -->
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-diamond pe-3x"></i> </div>
                            <h5 class="mt-0">Access tender dashboard</h5> All the retailers can make a tender on a specific product based on demands. This field gives new opportunity to offer your product and make a successful deal
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media">
                        <!-- // end .di -->
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-tools pe-3x"></i> </div>
                            <h5 class="mt-0">Marketing tools</h5> Platform gives opportunity to make marketing in a different manner, withing a minit. You can make personal deals, discount, cashback, directed ads and more with the help of specific tools.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media">
                        <!-- // end .di -->
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-map-2 pe-3x"></i> </div>
                            <h5 class="mt-0">Marketplace</h5>New marketplace, with rising amount of retailers. Map of the the marketplace gives reports on selles based on region, it also hels cross sales techniques. all of this is done for finding perfect segments for sales and maximizing efficiency.
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- // end features -->



    <!-- Testimonials -->
    <div class="section">
        <div class="container">
            <div class="section-title text-center">
                <h3>What our users say</h3>
                <p>Read what our customers had to say!</p>
            </div>

<!--
            <div class="row">
                <div class="col-md-4">
                    <div class="embed-tweet-item">
                        <blockquote class="twitter-tweet" data-cards="hidden" lang="en" data-width="550" data-link-color="#7642FF" data-align="center">
                            <a href="https://twitter.com/kmin/status/943914224329347072"></a>
                        </blockquote>
                    </div>
                     end .embed-tweet-item
                </div>
-->
    <!--

                <div class="col-md-4">
                    <div class="embed-tweet-item">
                        <blockquote class="twitter-tweet" data-cards="hidden" lang="en" data-width="550" data-link-color="#7642FF" data-align="center">
                            <a href="https://twitter.com/halarewich/status/954224121784688640"></a>
                        </blockquote>
                    </div>

                </div>

                -->

                <!--
                <div class="col-md-4">
                    <div class="embed-tweet-item">
                        <blockquote class="twitter-tweet" data-cards="hidden" lang="en" data-width="550" data-link-color="#7642FF" data-align="center">
                            <a href="https://twitter.com/scottbelsky/status/921417663859052544"></a>
                        </blockquote>
                    </div>
                     -->
                </div>
                <!-- end .col -->
            </div>
            <!-- end .row -->


        </div>
    </div>
    <!-- // end Testimonials -->






    <!-- Pricing -->
    <div class="section bg-light py-lg" id="pricing">
        <div class="container">

            <div class="section-title text-center mt-0 mb-5">
                <h3>Choose your plan</h3>
                <p>Simple pricing monthly plan. no hidden charges. Choose a plan fit your needs</p>
            </div>

            <div class="row">
                <div class="col-lg-4">
                    <div class="card pricing">
                        <div class="card-body">
                            <small class="text-muted">Standart</small>
                            <h5 class="card-title">₾210</h5>
                            <p class="card-text">
                                <ul class="list-unstyled">
                                    <li>Make orders</li>
                                    <li>Access to suppliers</li>
                                    <li>Reports &amp; analytics</li>
                                    <li>One user</li>
                                </ul>
                            </p>
                            <a href="#" class="btn btn-xl btn-outline-primary">Choose this plan</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card pricing">
                        <div class="card-body">
                            <small class="text-muted">Premium</small>
                            <h5 class="card-title">₾310</h5>
                            <p class="card-text">
                                <ul class="list-unstyled">
                                    <li>Make order</li>
                                    <li>Access to retailers</li>
                                    <li>Accounting &amp; Logistics</li>
                                    <li>Discounts &amp; Coupons</li>
                                </ul>
                            </p>
                            <a href="#" class="btn btn-xl btn-primary">Choose this plan</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card pricing">
                        <div class="card-body">
                            <small class="text-muted">Gold</small>
                            <h5 class="card-title">₾400</h5>
                            <p class="card-text">
                                <ul class="list-unstyled">
                                    <li>All premium futures</li>
                                    <li>Personal adds</li>
                                    <li>Marketing campaign</li>
                                    <li>Access to tenders</li>
                                </ul>
                            </p>
                            <a href="#" class="btn btn-xl btn-outline-primary">Choose this plan</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- // end Pricing -->

    <!-- Signup -->
    <div class="section" id="signup">
        <div class="container">
            <div class="section-title text-center">
                <h3>Start your free trial</h3>
                <p>Signup now. Its free and takes less than 3 minutes</p>
            </div>
            <div class="row justify-content-md-center">
                <div class="col col-md-5">
                    <form class="contact" action="{{URL::to('sendcontact')}}" method="post">
                       {{csrf_field()}}
                        <div class="form-group">
                            <input type="text" name="company" class="form-control" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Phone">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-xl btn-block btn-primary">Send Information</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>



    <div class="section bg-light mt-4" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4"> <img src="images/global/Easy%20Trade%201.png" class="logo-dark" alt="Start.ly Logo" />
                    <p class="mt-3 ml-1 text-muted"></p>
                    <p class="ml-1"><a href="https://themeforest.net/user/surjithctly/portfolio?ref=surjithctly&utm_source=footer_content" target="_blank"></a></p>
                    <!-- // end .lead -->
                </div>
                <!-- // end .col-sm-3 -->



                <!-- // end .col-sm-3 -->



                <!-- // end .col-sm-3 -->
                <div class="col-sm-2">
                    <ul class="list-unstyled footer-links ml-1">
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Linkedin</a></li>
                    </ul>
                </div>

                                              <div class="col-sm-2">
                    <ul class="list-unstyled footer-links ml-1">
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#about">About us</a></li>
                        <li><a href="#services">Services</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>

                               <div class="col-sm-2">
                    <ul class="list-unstyled footer-links ml-1">
<!--
                        <li><a href="#">Terms</a></li>
                        <li><a href="#about">Privacy</a></li>
-->
                    </ul>
                </div>
                <!-- // end .col-sm-3 -->
                <div class="col-sm-2">
                    <a href="#home" class="btn btn-sm btn-outline-primary ml-1">Go to Top</a>
                </div>
                <!-- // end .col-sm-3 -->
            </div>
            <!-- // end .row -->
<!--
            <div class=" text-center mt-4"> <small class="text-muted">Copyright ©
                          <script type="text/javascript">
                          document.write(new Date().getFullYear());
                          </script>
                          All rights reserved. Start.ly
                      </small></div>
        </div>
-->
        <!-- // end .container -->
    </div>
    <!-- // end #about.section -->
    <!-- // end .agency -->
    <!-- JS Files -->
    <!-- build:js js/app.min.js -->
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="{{URL::asset('public/assets/main_view/js/global/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{URL::asset('public/assets/main_view/js/global/bootstrap.bundle.min.js')}}"></script>
    <!-- Main JS -->
    <script src="{{URL::asset('public/assets/main_view/js/script.js')}}"></script>
    <!-- /build -->
</body>

</html>
