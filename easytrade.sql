-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 02, 2018 at 12:17 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easytrade`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_06_28_002701_create_t_c_table', 1),
(2, '2018_07_11_174829_create_users_table', 1),
(3, '2018_07_12_132544_create_organization_table', 1),
(4, '2018_07_12_140507_create_organization_type_table', 1),
(5, '2018_07_17_161815_create_organization_pictures_table', 1),
(6, '2018_07_17_174623_create_productions_table', 1),
(7, '2018_07_17_231957_create_quantity_formats_table', 1),
(8, '2018_08_07_121310_create_tenders_table', 1),
(9, '2018_08_07_130211_create_notifications_table', 1),
(10, '2018_08_11_221702_create_phones_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('5616b405-355e-452e-990e-6196f1c6453d', 'App\\Notifications\\TenderUpload', 'App\\User', 1, '{\"text\":\"your file apiSoap.pdf uploaded\"}', NULL, '2018-08-12 14:13:29', '2018-08-12 14:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `organizationID` int(11) DEFAULT NULL,
  `organization_typeID` int(11) DEFAULT NULL,
  `organization_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_slogan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `langID` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `organizationID`, `organization_typeID`, `organization_name`, `organization_slogan`, `description`, `langID`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'შპს საქართველოს მცენარეული ზეთების კომპანია ქარვა', NULL, NULL, 1, '2018-09-01 18:54:02', '2018-09-01 18:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `organization_pictures`
--

CREATE TABLE `organization_pictures` (
  `id` int(10) UNSIGNED NOT NULL,
  `organizationID` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organization_pictures`
--

INSERT INTO `organization_pictures` (`id`, `organizationID`, `image`, `default`, `created_at`, `updated_at`) VALUES
(1, 1, 'https://forum.manjaro.org/uploads/default/original/1X/c4e8fd8ad6c93137cd682b66595a3a7f6e93d5b4.png', '1', NULL, NULL),
(2, 1, 'https://forum.manjaro.org/uploads/default/original/1X/c4e8fd8ad6c93137cd682b66595a3a7f6e93d5b4.png', '0', NULL, NULL),
(3, 1, 'https://forum.manjaro.org/uploads/default/original/1X/c4e8fd8ad6c93137cd682b66595a3a7f6e93d5b4.png', '0', NULL, NULL),
(4, 2, 'https://forum.manjaro.org/uploads/default/original/1X/c4e8fd8ad6c93137cd682b66595a3a7f6e93d5b4.png', '1', NULL, NULL),
(5, 2, 'https://forum.manjaro.org/uploads/default/original/1X/c4e8fd8ad6c93137cd682b66595a3a7f6e93d5b4.png', '0', NULL, NULL),
(6, 2, 'https://forum.manjaro.org/uploads/default/original/1X/c4e8fd8ad6c93137cd682b66595a3a7f6e93d5b4.png', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization_types`
--

CREATE TABLE `organization_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `typeID` int(11) NOT NULL,
  `organization_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `langID` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organization_types`
--

INSERT INTO `organization_types` (`id`, `typeID`, `organization_type`, `langID`, `created_at`, `updated_at`) VALUES
(1, 1, 'magazia', 1, '2018-08-12 13:10:41', '2018-08-12 13:10:41');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `id` int(10) UNSIGNED NOT NULL,
  `userID` int(11) NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productions`
--

CREATE TABLE `productions` (
  `id` int(10) UNSIGNED NOT NULL,
  `organizationID` int(11) NOT NULL,
  `barcode` bigint(20) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` double(8,2) NOT NULL,
  `quantityFormatID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productions`
--

INSERT INTO `productions` (`id`, `organizationID`, `barcode`, `image`, `product_name`, `price`, `quantity`, `quantityFormatID`, `created_at`, `updated_at`) VALUES
(1, 1, 12345, 'https://scontent.ftbs5-1.fna.fbcdn.net/v/t1.0-9/39017848_1785578408164600_9209381253902499840_n.jpg?_nc_cat=0&oh=ae3e9c39ccaf4e5afd102ec2316f1948&oe=5BF53A6E', 'ragac', 55.00, 55.00, 1, NULL, NULL),
(2, 1, 12345, 'https://scontent.ftbs5-1.fna.fbcdn.net/v/t1.0-9/39017848_1785578408164600_9209381253902499840_n.jpg?_nc_cat=0&oh=ae3e9c39ccaf4e5afd102ec2316f1948&oe=5BF53A6E', 'ragac', 55.00, 55.00, 1, NULL, NULL),
(3, 2, 12345, 'https://scontent.ftbs5-1.fna.fbcdn.net/v/t1.0-9/39017848_1785578408164600_9209381253902499840_n.jpg?_nc_cat=0&oh=ae3e9c39ccaf4e5afd102ec2316f1948&oe=5BF53A6E', 'ragac', 55.00, 55.00, 1, NULL, NULL),
(4, 2, 12345, 'https://scontent.ftbs5-1.fna.fbcdn.net/v/t1.0-9/39017848_1785578408164600_9209381253902499840_n.jpg?_nc_cat=0&oh=ae3e9c39ccaf4e5afd102ec2316f1948&oe=5BF53A6E', 'ragac', 55.00, 55.00, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quantity_formats`
--

CREATE TABLE `quantity_formats` (
  `id` int(10) UNSIGNED NOT NULL,
  `quantityFormatID` int(11) NOT NULL,
  `quantityFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `langID` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quantity_formats`
--

INSERT INTO `quantity_formats` (`id`, `quantityFormatID`, `quantityFormat`, `langID`, `created_at`, `updated_at`) VALUES
(1, 1, 'kg', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tenders`
--

CREATE TABLE `tenders` (
  `id` int(10) UNSIGNED NOT NULL,
  `userID` int(11) NOT NULL,
  `productName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `quantity` double(8,2) NOT NULL,
  `quantityFormatID` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startTender` date NOT NULL,
  `endTender` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tenders`
--

INSERT INTO `tenders` (`id`, `userID`, `productName`, `price`, `quantity`, `quantityFormatID`, `file`, `startTender`, `endTender`, `created_at`, `updated_at`) VALUES
(1, 1, 'ragac', 12.00, 50.00, 1, 'http://127.0.0.1/easytrade/public/assets/tenderimages/1-1534097609-apiSoap.pdf', '2018-08-12', '2018-08-29', '2018-08-12 14:13:29', '2018-08-12 14:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `t_c`
--

CREATE TABLE `t_c` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` double(8,2) NOT NULL,
  `langID` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `t_c`
--

INSERT INTO `t_c` (`id`, `text`, `version`, `langID`, `created_at`, `updated_at`) VALUES
(1, 'test', 1.00, 1, NULL, NULL),
(2, 'test2', 1.00, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `identification_code` int(11) NOT NULL,
  `organization_nameID` int(11) DEFAULT NULL,
  `countryID` int(11) DEFAULT NULL,
  `cityID` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rs_su` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userOrCompany` int(11) NOT NULL,
  `t_c_ID` int(11) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `identification_code`, `organization_nameID`, `countryID`, `cityID`, `address`, `first_name`, `last_name`, `rs_su`, `email`, `userOrCompany`, `t_c_ID`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 204484717, 1, NULL, NULL, NULL, NULL, NULL, 'otarqavtaradze:405184349', NULL, 1, 2, '$2y$10$3aY13cFahMvXpYPpz5xd5.KQOfRM3P56C7JiwWVjdq/7MU.HP5uvq', '5WEIsE9nNvFCkZ3gEZRwpEPgsDLqxXuaiLwAyzgoDO1A4OgYJSvshC09gU0x', '2018-09-01 18:54:02', '2018-09-01 18:54:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_pictures`
--
ALTER TABLE `organization_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_types`
--
ALTER TABLE `organization_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productions`
--
ALTER TABLE `productions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quantity_formats`
--
ALTER TABLE `quantity_formats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tenders`
--
ALTER TABLE `tenders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_c`
--
ALTER TABLE `t_c`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_identification_code_unique` (`identification_code`),
  ADD UNIQUE KEY `users_rs_su_unique` (`rs_su`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organization_pictures`
--
ALTER TABLE `organization_pictures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `organization_types`
--
ALTER TABLE `organization_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productions`
--
ALTER TABLE `productions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quantity_formats`
--
ALTER TABLE `quantity_formats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tenders`
--
ALTER TABLE `tenders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_c`
--
ALTER TABLE `t_c`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
